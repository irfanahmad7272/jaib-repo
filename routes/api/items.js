const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');

// Item Model
const Item = require('../../models/Item');

// @route   GET api/items
// @desc    Get All Items
// @access  Public
router.get('/', (req, res) => {
  const pageOptions = {
    page: parseInt(req.query.page) || 0, 
    limit: parseInt(req.query.limit) || 2
    // offset: (page - 1) * 10 
    // offset = (page - 1) * itemsPerPage
  }
  
  Item.find()
    .sort({ date: -1 })
    // .skip(pageOptions.page * pageOptions.limit)
    // .limit(pageOptions.limit)
    .then(items => res.json(items))
    .catch(err => res.status(404).json({ nopostsfound: 'No posts found' }))
});

// @route   POST api/items
// @desc    Create An Item
// @access  Private
router.post('/', (req, res) => { 
  // console.log('add item', req.body)
  const newItem = new Item({
    name: req.body.name
  });
  newItem.save().then(item => res.json(item));
});

// @route   DELETE api/items/:id
// @desc    Delete A Item
// @access  Private
router.delete('/:id', auth, (req, res) => {
  Item.findById(req.params.id)
    .then(item => item.remove().then(() => res.json({ success: true })))
    .catch(err => res.status(404).json({ success: false }));
});

module.exports = router;
