import React , {useState , useEffect} from "react";
import { Route,  BrowserRouter , Redirect , Link} from "react-router-dom";

import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
// import AppBar from "@material-ui/core/AppBar";
// import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { fade, makeStyles } from "@material-ui/core/styles";

// import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";

// import InputBase from "@material-ui/core/InputBase";
// import Badge from "@material-ui/core/Badge";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
// import MenuIcon from "@material-ui/icons/Menu";
// import SearchIcon from "@material-ui/icons/Search";
// import AccountCircle from "@material-ui/icons/AccountCircle";
// import MailIcon from "@material-ui/icons/Mail";
// import NotificationsIcon from "@material-ui/icons/Notifications";
import MoreIcon from "@material-ui/icons/MoreVert";
import Icon from "@material-ui/core/Icon";
import logo from "../assets/logo.PNG";
// import myAccountLogo from "../assets/tools-solid.svg";
import "./header.css";

// import {
//   Collapse,
//   Navbar,
//   NavbarToggler,
//   NavbarBrand,
//   Nav,
//   NavItem,
//   NavLink,
//   UncontrolledDropdown,
//   DropdownToggle,
//   DropdownMenu,
//   DropdownItem,
//   NavbarText
// } from 'reactstrap';
import setBasePath from '../../utils/setbasePath';
import setAuthToken from '../../utils/setAuthToken'
import {loadUser , logout} from '../../redux/actions/authAction';
import {connect} from 'react-redux'
import InnerHeader from "./innerHeader";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
}));
 function Header(props) {
  useEffect(() => {
    setBasePath()
  let loaduser = localStorage.getItem('token')
  console.log('loaduser',loaduser)
   props.loadUser(loaduser)
   setAuthToken(loaduser)
  // console.log('loaduser',loaduser)
 
},[]);
 
  const classes = useStyles();
console.log('props', props.auth.user)
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  //inner header
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
      style={{display:'flex', color:'white', flexDirection:'row-reverse' }}
    >
   <Link to="/" id="handel-hover"> 
     <MenuItem>
        <p>Home</p>
      </MenuItem></Link>
      <Link  to="/myhome" id="handel-hover">  <MenuItem>
        <p>Demo</p>
      </MenuItem></Link>
      <Link  to="/tour" id="handel-hover">   <MenuItem>
        <p>How it Works</p>
      </MenuItem></Link>
      <Link  to="pricing" id="handel-hover">   <MenuItem>
        <p>Pricing</p>
      </MenuItem></Link>
      <Link  to="/download_win" id="handel-hover">  <MenuItem>
        <p>download</p>
      </MenuItem></Link>
      <Link  to="/support" id="handel-hover">  <MenuItem>
        <p>Support</p>
      </MenuItem></Link>
      <Link  to="/blog" id="handel-hover">  <MenuItem>
        <p>Blog</p>
      </MenuItem></Link>
    </Menu>
  );
  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        className="headerContainer"
       
      >
        <Grid item xs={6} style={{textAlign:'center'}}>
          {/* <img src={logo} id="handelLogoMobile" /> */}
          <h2 style={{color:'white'}}>Screentime Monitor</h2>
        </Grid>
        <Grid item xs={6}>
          {/* <AppBar position="static" className="headerContainer"> */}
          {/* <Toolbar> */}

          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
           <Link to="/" id="handel-hover"> <Typography className={classes.title} style={{ color: "white" }}>
              <Button color="inherit" className="text">
                Home
              </Button>
            </Typography>
            </Link>
            <Link to="/myhome" id="handel-hover">
            <Typography className={classes.title} style={{ color: "white" }}>
             <Button color="inherit" className="text">
                Demo
              </Button>
            </Typography>
            </Link>
          <Link to="/tour" id="handel-hover">  <Typography className={classes.title} style={{ color: "white" , width:100}}>
              <Button color="inherit" className="text">
                How it Work
              </Button>
            </Typography></Link>
         <Link to="/pricing" id="handel-hover">   <Typography className={classes.title} style={{ color: "white" }}>
              <Button color="inherit" className="text">
                Pricing
              </Button>
            </Typography></Link>
          <Link to="/download_win" id="handel-hover">  <Typography className={classes.title} style={{ color: "white" }}>
              <Button color="inherit" className="text">
                Download
              </Button>
            </Typography></Link>
          <Link to="/support" id="handel-hover">  <Typography className={classes.title} style={{ color: "white" }}>
              <Button color="inherit" className="text">
                Support
              </Button>
            </Typography></Link>
            <Link  to="/blog" id="handel-hover">   <Typography className={classes.title} style={{ color: "white" }}>
              <Button color="inherit" className="text">
                Blog
              </Button>
            </Typography>
            </Link>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              id="handelMobile"
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
          {/* </Toolbar> */}
          {/* </AppBar> */}
          {renderMobileMenu}
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            className="headerContainer"
          ></Grid>
          <Grid
            item
            xs={8}
            style={{ backgroundColor: "#16262F", borderRadius: 10 }}
          >
            {/* <div className={classes.grow} /> */}
           {props.auth.isAuthenticated == true ? <div className={classes.sectionDesktop}>
              <Typography
                className={classes.title}
                style={{
                  color: "#999",
                  fontSize: 14,
                  marginTop: 9,
                  marginLeft: 10,
                  paddingLeft: 20,
                  width:250
                }}
              >
                Hello,{props.auth.user}
              </Typography>
              <Typography
                className={classes.title}
                style={{
                  color: "white",
                  backgroundColor: "#16262",
                  padding: 3,
                }}
              >
              <Link to="/myhome" id="handel-hover">  <Button
                  variant="contained"
                  style={{ backgroundColor: "#16262F", color: "white" ,width:140 }}
                  className={classes.button}
                  startIcon={
                    <Icon
                      className="fa fa-user"
                      style={{ fontSize: 24, color: "white"  }}
                    />
                  }
                >
                  My Home
                </Button></Link>
              </Typography>

              <Typography
                className={classes.title}
                style={{ color: "white", padding: 2 }}
              >
                {/* <Icon className="fa fa-tools" style={{ fontSize: 12 }}/> */}
              <Link to="/account">  <Button
                  variant="contained"
                  style={{ backgroundColor: "#16262F", color: "white" ,width:140}}
                  className={classes.button}
                  startIcon={<Icon className="fa fa-cogs" />}
                >
                  My Account
                </Button>
                </Link>
              </Typography>
              <Typography
                className={classes.title}
                style={{ color: "white", backgroundColor: "#16262" }}
              >
                <Button
                  //  variant="contained"
                  style={{
                    backgroundColor: "#749455",
                    color: "white",
                    textTransform: "capitalize",
                    width:100
                  }}
                  // className={classes.button}
                  onClick={()=>props.logout()}
                >
                  Log out
                </Button>
              </Typography>
            </div> : <div style={{color:"white"}}>
              <span>Don't have an account?</span>
              <Link to="/signup">  <Button
                  variant="contained"
                  style={{ backgroundColor: "#16262F", color: "white" ,width:90}}
                  className={classes.button}
                  // startIcon={<Icon className="fa fa-cogs" />}S
                >
                  Signup
                </Button>
                </Link>
                <Link to="/login">  <Button
                  variant="contained"
                  style={{ backgroundColor: "#16262F", color: "white" ,width:80}}
                  className={classes.button}
                  // startIcon={<Icon className="fa fa-cogs" />}
                >
                  Login
                </Button>
                </Link>
            </div> }
          </Grid>
        </Grid>
      </Grid>
<InnerHeader/>
      
    </div>
  );
}
const mapStateToProps = state => ({
  auth: state.auth
});
const mapDispatchToProps = dispatch =>{
  return {
    loadUser : data => dispatch(loadUser(data)), 
    logout : () => dispatch(logout())

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header)