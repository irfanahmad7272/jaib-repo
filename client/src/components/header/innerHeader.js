import React, { useState, useEffect } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./header.css";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
 import { Link } from "react-router-dom";
 import './innerHeader.css';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));
export default function InnerHeader() {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  return (
 
    <div>
     <Navbar  light expand="md" id="handelNav">
        <Collapse isOpen={isOpen} navbar style={{backgroundColor:'#79975C', marginLeft:50}}>
          <Nav className="mr-auto" navbar style={{backgroundColor:'#79975C'}} >
         <Link to="/myhome" id="handel-hover">  <NavItem>
              <NavLink style={{ color: "white" }} id="handelInnerNav">My Home</NavLink>
            </NavItem></Link> 
            <UncontrolledDropdown nav inNavbar style={{backgroundColor:'#79975C', color: "white" }}>
              <DropdownToggle nav caret style={{ color: "white" }} id="handelInnerNav">
                Timeline
              </DropdownToggle>
              <DropdownMenu right>
            <Link to="/BrainMay" id="handel-hover" >
              <DropdownItem>
               Brain May
                </DropdownItem></Link>
             <Link to="/DemoManager" id="handel-hover">   <DropdownItem>
               Demo Manager
                </DropdownItem>
                </Link>
              <Link to="/james" id="handel-hover" >  <DropdownItem>
                  James Hetfield
                </DropdownItem></Link>
               <Link to="/till" id="handel-hover"> <DropdownItem>
                  Till lindemann
                </DropdownItem> </Link>
              <Link to="/victor" id="handel-hover">  <DropdownItem>
                 Vicktor Zinchuk
                </DropdownItem></Link>
              </DropdownMenu>
            </UncontrolledDropdown>
        
           
            <UncontrolledDropdown nav inNavbar id="drop" style={{backgroundColor:'#79975C', color: "white" }}>
               <DropdownToggle nav caret style={{  color: "white" }} id="handelInnerNav">
                Reports
              </DropdownToggle> 
              
              <DropdownMenu right >
              <Link to="/summary" id="handel-hover">  <DropdownItem>
               Summary
                </DropdownItem></Link>
              <Link to="/report_details" id="handel-hover">  <DropdownItem>
              Detailed
                </DropdownItem></Link>
                 <DropdownItem divider /> 
              <Link to="/savereports" id="handel-hover"> <DropdownItem>
                  Saved Reports
                </DropdownItem>
                </Link> 
               
              </DropdownMenu>
            </UncontrolledDropdown>
         <Link to="/team" id="handel-hover">   <NavbarText style={{ color: "white" }} id="handelInnerNav">Team</NavbarText></Link>
            <UncontrolledDropdown nav inNavbar id="drop" style={{backgroundColor:'#79975C', color: "white" }}>
              <DropdownToggle nav caret style={{  color: "white" }} id="handelInnerNav">
              
              Projects
         
              </DropdownToggle>
              <DropdownMenu right>
               
              <Link to="/client" id="handel-hover">  <DropdownItem>
              Clients
                </DropdownItem></Link>
                 <DropdownItem divider /> 
                 <Link to="/setting" id="handel-hover">      <DropdownItem>
                  Settings
                </DropdownItem></Link>
              </DropdownMenu>
            </UncontrolledDropdown>
           
          </Nav>
          
        </Collapse>
      </Navbar> 
    </div>
  );
}
