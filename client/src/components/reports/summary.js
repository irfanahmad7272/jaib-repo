import React from "react";
import './summary.css'
import {Link} from 'react-router-dom'
export default function Summary() {
  return (
    <div id="content_wrapper">
      <div id="content_wrapper_inner" style={{ position:'relative', top:-46}}>
        <div id="react-view">
          {/* start  */}
          <div>
            {/* 1st div  */}
            <div class="joyride">
              <button class="joyride-beacon">
                <span class="joyride-beacon__inner"></span>
                <span class="joyride-beacon__outer"></span>
              </button>
            </div>
            {/* 2nd div  */}
            <div id="page_title">
              <h1 style={{ display: "inline-block" }}>Reports</h1>
            </div>
            {/* 3rd div  */}
            <div class="reports_container">
              <div class="filter_container">
                {/* input one start  */}
                <div class="search-row" style={{display:"flex"}}>
                  <div style={{width:413}}>
                    <div class="moment-date-range">
                      <div class="inputs">
                        <input
                          type="text"
                          placeholder="Start Date"
                          class="input-from"
                          value="01/01/20"
                        />
                        &nbsp;&nbsp;►&nbsp;&nbsp;
                        <input
                          type="text"
                          placeholder="End Date"
                          class="input-to"
                          value="12/31/20"
                        />
                      </div>
                    </div>
                  </div>
                  <table class="timeIntervals">
                    <tbody>
                      <tr>
                        <td>
                          <Link class="">Today</Link>
                        </td>
                        <td>
                          <Link class="">This Week</Link>
                        </td>
                        <td>
                          <Link class="">This Month</Link>
                        </td>
                        <td>
                          <Link class=" bold">This Year</Link>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <Link class="">Yesterday</Link>
                        </td>
                        <td>
                          <Link class="">Last Week</Link>
                        </td>
                        <td>
                          <Link class="">Last Month</Link>
                        </td>
                        <td>
                          <Link class="">Last Year</Link>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="selector-container" tabindex="0" style={{fontSize:12, width:112}}>
                   
                     Report times
                      {/* <div class="selector-dropdown">
                        
                      </div> */}
                    
                  </div>
                </div>
                {/* input one end  */}
                {/* input two start  */}
                <div class="search-row">
                  <div class="Select Select--multi is-clearable is-searchable">
                    <div class="Select-control">
                      <span
                        class="Select-multi-value-wrapper"
                        id="react-select-6--value"
                      >
                        {/* <div class="Select-placeholder">
                          Select employees and groups
                        </div> */}
                        <div class="Select-input">
                          <input
                          type="text"
                          placeholder="Select employees and groups"
                            // role="combobox"
                            // aria-expanded="false"
                            // aria-owns=""
                            // aria-haspopup="false"
                            // aria-activedescendant="react-select-6--value"
                            // value=""
                          />
                          <div></div>
                        </div>
                      </span>
                      <span class="Select-arrow-zone">
                        <span class="Select-arrow"></span>
                      </span>
                    </div>
                  </div>
                </div>
                {/* input two end  */}
                {/* input three start  */}
                <div class="search-row">
                  <div class="Select Select--multi is-clearable is-searchable">
                    <div class="Select-control">
                      <span
                        class="Select-multi-value-wrapper"
                        id="react-select-7--value"
                      >
                        {/* <div class="Select-placeholder">Select clients</div> */}
                        <div class="Select-input">
                          <input
                           type="text"
                           placeholder="Select clients"
                            // role="combobox"
                            // aria-expanded="false"
                            // aria-owns=""
                            // aria-haspopup="false"
                            // aria-activedescendant="react-select-7--value"
                            // value=""
                          />
                          <div></div>
                        </div>
                      </span>
                      <span class="Select-arrow-zone">
                        <span class="Select-arrow"></span>
                      </span>
                    </div>
                  </div>
                </div>
                {/* input three end  */}
                {/* input four start  */}
                <div class="search-row">
                  <div class="Select Select--multi is-clearable is-searchable">
                    <div class="Select-control">
                      <span
                        class="Select-multi-value-wrapper"
                        id="react-select-8--value"
                      
                      >
                        {/* <div class="Select-placeholder">Select projects</div> */}
                        <div class="Select-input">
                          <input
                          type="text"
                          placeholder="Select projects"
                          
                            // role="combobox"
                            // aria-expanded="false"
                            // aria-owns=""
                            // aria-haspopup="false"
                            // aria-activedescendant="react-select-8--value"
                            // value=""
                          />
                          <div></div>
                        </div>
                      </span>
                      <span class="Select-arrow-zone">
                        <span class="Select-arrow"></span>
                      </span>
                    </div>
                  </div>
                </div>
                {/* input four end  */}
                {/* input five start  */}
                <div class="search-row">
                  <input
                    type="text"
                    class="notesInput"
                    value=""
                    placeholder="Note contains text"
                  />
                </div>
                {/* input five end  */}
                {/* input six start  */}
                {/* <div class="search-row">
                  <input
                    type="text"
                    class="notesInput"
                    value=""
                    placeholder="Note contains text"
                  />
                </div> */}
                {/* input six end  */}
                {/* input seven start  */}
               {/*   <div class="search-row">
                  <div>
                    <Link class="weakLink">Summary by project</Link>
                    <Link class="weakLink">Summary by client</Link>
                    <Link class="weakLink">Summary by employee</Link>
                    <Link class="weakLink">Daily by employee</Link>
                    <Link class="weakLink">Detailed</Link>
                    <Link class="weakLink">Apps &amp; URLs</Link>
                  </div>
                 <div class="Select Select--multi is-clearable is-searchable has-value">
                    <div class="Select-control">
                      <span
                        class="Select-multi-value-wrapper"
                        id="react-select-9--value"
                      >
                        <div class="Select-value">
                          <span class="Select-value-icon" aria-hidden="true">
                            ×
                          </span>
                          <span
                            class="Select-value-label"
                            role="option"
                            aria-selected="true"
                            id="react-select-9--value-0"
                          >
                            Group by employee
                            <span class="Select-aria-only">&nbsp;</span>
                          </span>
                        </div>
                        <div class="Select-value">
                          <span class="Select-value-icon" aria-hidden="true">
                            ×
                          </span>
                          <span
                            class="Select-value-label"
                            role="option"
                            aria-selected="true"
                            id="react-select-9--value-1"
                          >
                            Group by project
                            <span class="Select-aria-only">&nbsp;</span>
                          </span>
                        </div>
                        <div class="Select-input">
                          <input
                            role="combobox"
                            aria-expanded="false"
                            aria-owns=""
                            aria-haspopup="false"
                            aria-activedescendant="react-select-9--value"
                            value=""
                          />
                          <div></div>
                        </div>
                      </span>
                      <span
                        class="Select-clear-zone"
                        title="Clear all"
                        aria-label="Clear all"
                      >
                        <span class="Select-clear">×</span>
                      </span>
                      <span class="Select-arrow-zone">
                        <span class="Select-arrow"></span>
                      </span>
                    </div>
                  </div>
                </div> */}

                {/* input seven end  */}
              </div>
            </div>
            {/* main second div start */}
            <div class="chartsContainer" id="chartsContainer_p">
              <div class="linkContainer">
                <Link to="/" class="reportChart">
                  Timeline
                </Link>
                <Link to="/" class="reportChart active">
                  Employees
                </Link>
                <Link to="/" class="reportChart">
                  Projects
                </Link>
                <Link to="/" class="reportChart">
                  Clients
                </Link>
                <Link to="/" class="reportChart">
                  Notes
                </Link>
                <Link to="/" class="reportChart">
                  Apps &amp; URLs
                </Link>
              </div>
              {/* inner second start  */}
              <div class="charts">
                <div class="total">
                  <div class="totalHours">221h 42m</div>
                  <div class="totalMoney">$1896.85</div>
                  <div class="activityAverage">49%</div>
                </div>
                <div>
                  <div class="chart">
                    <div id="chart1" d3pie="0.1.6">
                      <svg width="830" height="300">
                        <text
                          x="415"
                          y="300"
                          id="p227_footer"
                          class="p227_footer"
                          text-anchor="left"
                          fill="#666666"
                        
                        ></text>
                        <g class="p227_lineGroups" >
                          <g class="p227_lineGroup">
                            <path
                              d="M587.9692512238156,81.62245707956674L589.9601491710777,79.0582992200505C591.9510471183396,76.49414136053424,595.9328430128637,71.36582564150174,599.9146389073878,67.48028182881342C603.8964348019119,63.59473801612509,607.878230696436,60.951966109780926,609.8691286436981,59.63058015660884L611.8600265909602,58.309194203436775"
                              stroke="#3366CC"
                              stroke-width="1"
                              fill="none"
                       
                            ></path>
                          </g>
                          <g class="p227_lineGroup">
                            <path
                              d="M580.981179743057,225.14308963380728L582.1392778231785,226.08237825422987C583.2973759033001,227.02166687465242,585.6135720635432,228.9002441154976,589.2459643840294,232.1907319305213C592.8783567045156,235.481219745545,597.8269451852449,240.1836181347472,600.3012394256095,242.53481732934833L602.7755336659742,244.88601652394942"
                              stroke="#DC3912"
                              stroke-width="1"
                              fill="none"
                           
                            ></path>
                          </g>
                          <g class="p227_lineGroup">
                            <path
                              d="M440.54661028030114,216.75846582454272L439.5326095754715,217.5929466473495C438.51860887064197,218.4274274701563,436.4906074609828,220.09638911576985,432.4346046416645,222.9676457403303C428.37860182234624,225.83890236489077,422.29459759336885,229.91245396839815,419.25259547888015,231.94922977015182L416.21059336439146,233.98600557190554"
                              stroke="#FF9900"
                              stroke-width="1"
                              fill="none"
                         
                            ></path>
                          </g>
                          <g class="p227_lineGroup">
                            <path
                              d="M445.86788980316646,77.74523310028003L443.02543567078516,75.03567934154053C440.1829815384039,72.32612558280103,434.49807327364135,66.90701806532203,430.7081344304664,62.82761305366937C426.9181955872914,58.7482080420167,425.0232261657039,56.00850553619037,424.07574145491014,54.638654283277205L423.1282567441164,53.26880303036404"
                              stroke="#109618"
                              stroke-width="1"
                              fill="none"
                            
                            ></path>
                          </g>
                        </g>
                        <g transform="translate(515,150)" class="p227_pieChart">
                          <g class="p227_arc" transform="rotate(0)">
                            <path
                              id="p227_segment0"
                              fill="#3366CC"
                            
                              d="M6.123233995736766e-15,-100A100,100 0 0,1 99.7891621485665,6.490232483286162L49.89458107428325,3.245116241643081A50,50 0 0,0 3.061616997868383e-15,-50Z"
                              data-index="0"
                            ></path>
                          </g>
                          <g
                            class="p227_arc"
                            transform="rotate(93.72124492557509)"
                          >
                            <path
                              id="p227_segment1"
                              fill="#DC3912"
                           
                              d="M6.123233995736766e-15,-100A100,100 0 0,1 99.99999721108126,-0.023617445680246842L49.99999860554063,-0.011808722840123421A50,50 0 0,0 3.061616997868383e-15,-50Z"
                              data-index="1"
                            ></path>
                          </g>
                          <g
                            class="p227_arc"
                            transform="rotate(183.70771312584571)"
                          >
                            <path
                              id="p227_segment2"
                              fill="#FF9900"
                           
                              d="M6.123233995736766e-15,-100A100,100 0 0,1 99.97889141661216,-2.05457321730735L49.98944570830608,-1.027286608653675A50,50 0 0,0 3.061616997868383e-15,-50Z"
                              data-index="2"
                            ></path>
                          </g>
                          <g
                            class="p227_arc"
                            transform="rotate(272.53044654939106)"
                          >
                            <path
                              id="p227_segment3"
                              fill="#109618"
                           
                              d="M6.123233995736766e-15,-100A100,100 0 0,1 99.90249015115918,-4.415026794657379L49.95124507557959,-2.2075133973286896A50,50 0 0,0 3.061616997868383e-15,-50Z"
                              data-index="3"
                            ></path>
                          </g>
                        </g>
                        <g class="p227_labels-inner">
                          <g
                            id="p227_labelGroup0-inner"
                            data-index="0"
                            class="p227_labelGroup-inner"
                            transform="translate(561.6913620629676,99.5674666174408)"
                         
                          >
                            <text
                              id="p227_segmentPercentage0-inner"
                              class="p227_segmentPercentage-inner"
                         
                            >
                              26%
                            </text>
                          </g>
                          <g
                            id="p227_labelGroup1-inner"
                            data-index="1"
                            class="p227_labelGroup-inner"
                            transform="translate(556.256195355711,211.19462527073898)"
                       
                          >
                            <text
                              id="p227_segmentPercentage1-inner"
                              class="p227_segmentPercentage-inner"
                           
                            >
                              25%
                            </text>
                          </g>
                          <g
                            id="p227_labelGroup2-inner"
                            data-index="2"
                            class="p227_labelGroup-inner"
                            transform="translate(447.0293079957898,204.67325119686657)"
                        
                          >
                            <text
                              id="p227_segmentPercentage2-inner"
                              class="p227_segmentPercentage-inner"
                          
                            >
                              25%
                            </text>
                          </g>
                          <g
                            id="p227_labelGroup3-inner"
                            data-index="3"
                            class="p227_labelGroup-inner"
                            transform="translate(451.1680809580184,96.55184796688447)"
                          
                          >
                            <text
                              id="p227_segmentPercentage3-inner"
                              class="p227_segmentPercentage-inner"
                           
                            >
                              24%
                            </text>
                          </g>
                        </g>
                        <g class="p227_labels-outer">
                          <g
                            id="p227_labelGroup0-outer"
                            data-index="0"
                            class="p227_labelGroup-outer"
                            transform="translate(617.8600265909602,61.10919420343677)"
                          
                          >
                            <text
                              id="p227_segmentMainLabel0-outer"
                              class="p227_segmentMainLabel-outer"
                            
                            >
                              Viktor Zinchuk — 57h 43m
                            </text>
                          </g>
                          <g
                            id="p227_labelGroup1-outer"
                            data-index="1"
                            class="p227_labelGroup-outer"
                            transform="translate(608.7755336659742,247.68601652394943)"
                          
                          >
                            <text
                              id="p227_segmentMainLabel1-outer"
                              class="p227_segmentMainLabel-outer"
                            
                            >
                              James Hetfield — 55h 25m
                            </text>
                          </g>
                          <g
                            id="p227_labelGroup2-outer"
                            data-index="2"
                            class="p227_labelGroup-outer"
                            transform="translate(267.24184336439146,236.78600557190555)"
                          
                          >
                            <text
                              id="p227_segmentMainLabel2-outer"
                              class="p227_segmentMainLabel-outer"
                             
                            >
                              Till Lindemann — 54h 42m
                            </text>
                          </g>
                          <g
                            id="p227_labelGroup3-outer"
                            data-index="3"
                            class="p227_labelGroup-outer"
                            transform="translate(297.7376317441164,56.06880303036404)"
                          
                          >
                            <text
                              id="p227_segmentMainLabel3-outer"
                              class="p227_segmentMainLabel-outer"
                             
                            >
                              Brian May — 53h 52m
                            </text>
                          </g>
                        </g>
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
              {/* inner second end  */}
              {/* inner thirt start  */}
              <div class="reportBodyContainer" >
                <table class="rpt-data group">
                  <thead>
                    <tr>
                      <th>
                        <Link to="#">± Employee</Link> / <Link to="#">± Project</Link>
                      </th>
                      <th class="durationeColumn">Duration</th>
                      <th class="moneyColumn">Money</th>
                      <th class="activityColumn">Activity</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="group-0" tabindex="0">
                      <td class="groupTitle">
                        <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                        Brian May
                      </td>
                      <td class="durationColumn">53h 52m</td>
                      <td class="moneyColumn">$938.67</td>
                      <td class="activityColumn">50%</td>
                    </tr>
                    <tr class="data data-0">
                      <td class="projectColumn">Administration ● Amazon</td>
                      <td class="durationColumn">27h 44m</td>
                      <td class="moneyColumn">$416.00</td>
                      <td class="activityColumn">51%</td>
                    </tr>
                    <tr class="data data-0" >
                      <td class="projectColumn">Development ● Microsoft</td>
                      <td class="durationColumn">26h 08m</td>
                      <td class="moneyColumn">$522.67</td>
                      <td class="activityColumn">49%</td>
                    </tr>
                    <tr class="group-0" tabindex="0">
                      <td class="groupTitle">
                        <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                        James Hetfield
                      </td>
                      <td class="durationColumn">55h 25m</td>
                      <td class="moneyColumn">$554.17</td>
                      <td class="activityColumn">50%</td>
                    </tr>
                    <tr class="data data-0" >
                      <td class="projectColumn">Administration ● Amazon</td>
                      <td class="durationColumn">30h 30m</td>
                      <td class="moneyColumn">$305.00</td>
                      <td class="activityColumn">50%</td>
                    </tr>
                    <tr class="data data-0" >
                      <td class="projectColumn">Development ● Microsoft</td>
                      <td class="durationColumn">24h 55m</td>
                      <td class="moneyColumn">$249.17</td>
                      <td class="activityColumn">49%</td>
                    </tr>
                    <tr class="group-0" tabindex="0">
                      <td class="groupTitle">
                        <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                        Till Lindemann
                      </td>
                      <td class="durationColumn">54h 42m</td>
                      <td class="moneyColumn"></td>
                      <td class="activityColumn">50%</td>
                    </tr>
                    <tr class="data data-0" >
                      <td class="projectColumn">Administration ● Amazon</td>
                      <td class="durationColumn">32h 42m</td>
                      <td class="moneyColumn"></td>
                      <td class="activityColumn">51%</td>
                    </tr>
                    <tr class="data data-0" >
                      <td class="projectColumn">Development ● Microsoft</td>
                      <td class="durationColumn">22h 00m</td>
                      <td class="moneyColumn"></td>
                      <td class="activityColumn">49%</td>
                    </tr>
                    <tr class="group-0" tabindex="0">
                      <td class="groupTitle">
                        <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                        Viktor Zinchuk
                      </td>
                      <td class="durationColumn">57h 43m</td>
                      <td class="moneyColumn">$404.02</td>
                      <td class="activityColumn">48%</td>
                    </tr>
                    <tr class="data data-0" >
                      <td class="projectColumn">Administration ● Amazon</td>
                      <td class="durationColumn">20h 22m</td>
                      <td class="moneyColumn">$142.57</td>
                      <td class="activityColumn">45%</td>
                    </tr>
                    <tr class="data data-0" >
                      <td class="projectColumn">Development ● Microsoft</td>
                      <td class="durationColumn">37h 21m</td>
                      <td class="moneyColumn">$261.45</td>
                      <td class="activityColumn">51%</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              {/* inner third end  */}
            </div>
            {/* main second div end */}
          </div>
          {/* end  */}
        </div>
      </div>
    </div>
  );
}
