import React from "react";
import {Link} from 'react-router-dom'
import './saveReport.css'
export default function SaveReports() {
  return (
    <div>
      <div id="content_wrapper">
        <div id="content_wrapper_inner">
          <div id="react-view">
            <div>
              <div id="page_title">
                <h1 style={{ display: "inline-block" }}>Saved reports</h1>
              </div>
              <div class="savedReports_container noData" style={{textAlign:'center'}}>
                <div>
                  Oops! No saved reports. Run a <Link href=""> report</Link> and click
                  Save report.
                </div>
                <img style={{width:'80%'}} src="https://screenshotmonitor.com/Content/img/home/saveReport.png" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
