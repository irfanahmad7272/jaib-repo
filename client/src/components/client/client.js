import React from "react";
import "./client.css";
import {Link} from 'react-router-dom'
export default function Client() {
  return (
    <div id="page">
      <div id="page_inner">
<div id="content_wrapper">
    <div id="content_wrapper_inner">
    <div id="react-view">
        <div>
          <div id="page_title">
            <h1>Clients</h1>
          </div>
          <div class="container__clients">
            <div class="container__clients__list">
              {/* <div>
                No clients yet. Start by creating one.
                <br></br>Then assign projects to clients and you'll be able to
                run reports to see time spent on each client.
              </div> */}
               <div class="container__clients__list-item container__clients__list-item-active">
                <span class="container__clients-index">1</span>
                <span class="truncated">Amazon</span>
              </div> 
              <div class="container__clients__list-item">
                <span class="container__clients-index">2</span>
                <span class="truncated">Microsoft</span>
              </div>
              <div>
                <div>
                  <form>
                    <div class="input-group" style={{display:'flex'}}>
                      <input
                      style={{width:220}}
                        type="text"
                        required=""
                        maxlength="50"
                        class="form-control"
                        placeholder="New client name..."
                        value=""
                      />
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="submit" style={{backgroundColor:'#D5D5D5'}}>
                          Create
                        </button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="container__clients__details">
              <div class="employee_details">
                <div class="employee__name" style={{display:'flex'}}>
                  <div>
                    <div class="clientName">
                      <span class="name trimmedName">
                        Amazon
                        <Link href="" class="changeName">
                          <i class="fa fa-pencil"></i>
                        </Link>
                      </span>
                    </div>
                    {/* <div class="clientName">
                      <input class="nameInput" maxlength="50" value="" />
                    </div> */}
                  </div>
                  <div class="actionButtons" style={{marginLeft:25}}>
                    <i class="fa fa-times"></i>
                    <Link to="#">Delete</Link>
                  </div>
                </div>
                <div>
                  Assign <Link >projects</Link> to this client and run
                  reports to see time spent on this client's projects.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
      </div>
    </div>
  );
}
