import React from "react";
import './team.css';
import {Link} from 'react-router-dom'
export default function Team() {
  return (
    <div>
      <div id="content_wrapper">
        <div id="content_wrapper_inner">
          <div id="react-view">
            <div>
              <div id="page_title">
                <h1>Team</h1>
              </div>
              <div class="tpl-team">
                <div class="container__team">
                  <div class="container__team__list">
                    <div class="userGroup">
                      <Link to="" id="create_group">
                        + Create user group
                        <i
                          class="fa fa-question-circle"
                          aria-hidden="true"
                          title=""
                          data-original-title="You will be able to select user groups in reports"
                        >
                          <i></i>
                        </i>
                      </Link>
                      <div id="handel_form">
                        <form style={{paddingTop:10}}>
                          <div class="input-group">
                            <input
                              required=""
                              maxlength="250"
                              value=""
                              class="form-control"
                              placeholder="New user group name"
                            />
                            <span class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                Create
                              </button>
                            </span>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div class="container__team__list-item container__team__list-item-active">
                      <span class="container__team-index">1</span>
                      <span class="truncated">Brian May</span>
                      <span class="container__team__list-item-value"></span>
                    </div>
                    <div class="container__team__list-item">
                      <span class="container__team-index">2</span>
                      <span class="truncated">Demo Manager</span>
                      <span class="container__team__list-item-value">
                        <div
                          class="manager"
                          title=""
                          data-original-title="Owner"
                        >
                          <i></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                      </span>
                    </div>
                    <div class="container__team__list-item">
                      <span class="container__team-index">3</span>
                      <span class="truncated">James Hetfield</span>
                      <span class="container__team__list-item-value"></span>
                    </div>
                    <div class="container__team__list-item">
                      <span class="container__team-index">4</span>
                      <span class="truncated">Till Lindemann</span>
                      <span class="container__team__list-item-value"></span>
                    </div>
                    <div class="container__team__list-item">
                      <span class="container__team-index">5</span>
                      <span class="truncated">Viktor Zinchuk</span>
                      <span class="container__team__list-item-value"></span>
                    </div>
                    <div class="addNewEmployee" id="addNewEmployee">
                      <div>
                        <form style={{paddingTop:10}}>
                          <div class="input-group" style={{display:'flex'}}>
                            <input
                              type="email"
                              required=""
                              maxlength="250"
                              value=""
                              class="form-control"
                              id="form-control-input"
                              placeholder="Add new employee by email"
                            />
                            <span class="input-group-btn" style={{backgroundColor:'#D5D5D5'}}>
                              <button class="btn btn-default" type="submit">
                                Invite
                              </button>
                            </span>
                          </div>
                        </form>
                      </div>
                      {/* <div>
                        <div>
                          We will send them an invitation email to download the
                          app and start the time tracking for your company.
                        </div>
                      </div> */}
                    </div>
                  </div>
                  <div class="container__team__details">
                    <div class="employee_details" style={{textAlign:'initial'}}>
                      <div class="joyride">
                        <button class="joyride-beacon">
                          <span class="joyride-beacon__inner"></span>
                          <span class="joyride-beacon__outer"></span>
                        </button>
                      </div>
                      <div class="employee__name">
                        <div>
                          <div>
                            <span class="name trimmedName">
                              Brian May
                              <Link to="" class="changeName">
                                <i class="fa fa-pencil"></i>
                              </Link>
                            </span>
                            {/* <span class="name trimmedName">Brian May</span> */}
                          </div>
                          {/* <div>
                            <input class="nameInput" maxlength="50" value="" />
                          </div> */}
                        </div>
                        <div class="actionButtons">
                          <span>
                            <i class="fa fa-pause"></i>
                            <Link
                              to=""
                              title="Disable to track time for your company"
                            >
                              Pause
                            </Link>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                          </span>
                          <i class="fa fa-archive"></i>
                          <Link to="">Archive</Link>&nbsp;&nbsp;&nbsp;&nbsp;
                          <i class="fa fa-times"></i>
                          <Link to="">Delete</Link>
                        </div>
                      </div>
                      <div class="employeeEmail">
                        brian@screentime monitor.com
                        <span
                          title=""
                          data-original-title="GB. IP: 91.109.242.36"
                        >
                          <i></i>
                          <img
                            class="employeeFlag"
                            src="https://screenshotmonitor.com/content/img/flags/GB.png"
                          />
                        </span>
                      </div>
                      <div style={{display:"none", marginTop:5}}>Archived </div>
                      <div class="optionRow epayrate">
                        <span>
                          <div>
                            <span>
                              <span>15$/hr </span>
                              <Link to="" title="Visible to this employee">
                                <span class="change">change</span>
                                {/* <span class="change">Set pay rate</span> */}
                              </Link>
                            </span>
                            {/* <span class="rate-edit">
                              <input type="number" step="any" value="" />
                              &nbsp;$/hr
                            </span> */}
                          </div>
                          <Link to="">View timeline</Link>
                        </span>
                      </div>
                      <div class="role">
                        <h4 class="name">Role</h4>
                        <div class="radio radio-success">
                          <input
                            type="radio"
                            id="user"
                             name="user"
                             value="on"
                          />
                          <label for="user">
                            User{" "}
                            <span class="info">
                              {" "}
                              - can see their own data only
                            </span>
                          </label>
                        </div>
                        <div class="radio radio-success">
                          <input
                            type="radio"
                            id="manager"
                            name="user"
                            value="on"
                          />
                          <label for="manager">
                            Manager
                            <span class="info">
                              {" "}
                              - can see selected user's Timeline &amp; Reports
                              (but not rates)
                            </span>
                          </label>
                        </div>
                        <div class="radio radio-success">
                          <input
                            type="radio"
                            id="admin"
                            name="user"
                            value="on"
                          />
                          <label for="admin">
                            Admin
                            <span class="info">
                              {" "}
                              - full control over Team, Projects &amp; Settings.
                              Does not have access to owner's "My Account" page
                              settings.
                            </span>
                          </label>
                        </div>
                      </div>
                      <div class="managersContainer">
                        <h4>Manager for</h4>
                        <div class="description">
                          If enabled, Brian May will be able to see selected
                          user's Timeline and Reports, but not rates.
                        </div>
                        <div class="managedUsers">
                          {/* <div class="managerRow">
                            <span class="managerValue">
                              <div class="react-toggle">
                                <div class="react-toggle-track">
                                  <div class="react-toggle-track-check">
                                    <svg
                                      width="14"
                                      height="11"
                                      viewBox="0 0 14 11"
                                    >
                                      <title>switch-check</title>
                                    </svg>
                                  </div>
                                  <div class="react-toggle-track-x">
                                    <svg
                                      width="10"
                                      height="10"
                                      viewBox="0 0 10 10"
                                    >
                                      <title>switch-x</title>
                                    </svg>
                                  </div>
                                </div>
                                <div class="react-toggle-thumb"></div>
                                <input
                                  type="checkbox"
                                  class="react-toggle-screenreader-only"
                                  value="on"
                                />
                              </div>
                            </span>
                            <span class="employeeName trimmedName noManager">
                              Demo Manager
                            </span>
                          </div> */}
                          {/* <div class="managerRow">
                            <span class="managerValue">
                              <div class="react-toggle">
                                <div class="react-toggle-track">
                                  <div class="react-toggle-track-check">
                                    <svg
                                      width="14"
                                      height="11"
                                      viewBox="0 0 14 11"
                                    >
                                      <title>switch-check</title>
                                    </svg>
                                  </div>
                                  <div class="react-toggle-track-x">
                                    <svg
                                      width="10"
                                      height="10"
                                      viewBox="0 0 10 10"
                                    >
                                      <title>switch-x</title>
                                    </svg>
                                  </div>
                                </div>
                                <div class="react-toggle-thumb"></div>
                                <input
                                  type="checkbox"
                                  class="react-toggle-screenreader-only"
                                  value="on"
                                />
                              </div>
                            </span>
                            <span class="employeeName trimmedName noManager">
                              James Hetfield
                            </span>
                          </div> */}
                          {/* <div class="managerRow">
                            <span class="managerValue">
                              <div class="react-toggle">
                                <div class="react-toggle-track">
                                  <div class="react-toggle-track-check">
                                    <svg
                                      width="14"
                                      height="11"
                                      viewBox="0 0 14 11"
                                    >
                                      <title>switch-check</title>
                                    </svg>
                                  </div>
                                  <div class="react-toggle-track-x">
                                    <svg
                                      width="10"
                                      height="10"
                                      viewBox="0 0 10 10"
                                    >
                                      <title>switch-x</title>
                                    </svg>
                                  </div>
                                </div>
                                <div class="react-toggle-thumb"></div>
                                <input
                                  type="checkbox"
                                  class="react-toggle-screenreader-only"
                                  value="on"
                                />
                              </div>
                            </span>
                            <span class="employeeName trimmedName noManager">
                              Till Lindemann
                            </span>
                          </div> */}
                          {/* <div class="managerRow">
                            <span class="managerValue">
                              <div class="react-toggle">
                                <div class="react-toggle-track">
                                  <div class="react-toggle-track-check">
                                    <svg
                                      width="14"
                                      height="11"
                                      viewBox="0 0 14 11"
                                    >
                                      <title>switch-check</title>
                                    </svg>
                                  </div>
                                  <div class="react-toggle-track-x">
                                    <svg
                                      width="10"
                                      height="10"
                                      viewBox="0 0 10 10"
                                    >
                                      <title>switch-x</title>
                                    </svg>
                                  </div>
                                </div>
                                <div class="react-toggle-thumb"></div>
                                <input
                                  type="checkbox"
                                  class="react-toggle-screenreader-only"
                                  value="on"
                                />
                              </div>
                            </span>
                            <span class="employeeName trimmedName noManager">
                              Viktor Zinchuk
                            </span>
                          </div>*/}
                        </div> 
                      </div>
                      <div class="managersContainer employeeProjects">
                        {/* <div>
                          <h4 class="projects">
                            Projects
                            <span>
                              <div class="checkbox checkbox-inline blurScreens checkbox-mini">
                                <input
                                  type="checkbox"
                                  id="payRateCheck"
                                  value="on"
                                />
                                <label for="payRateCheck">
                                  Use per project pay rates
                                </label>
                              </div>
                            </span>
                            <div>
                              <Link to="">Add all</Link>&nbsp;&nbsp;&nbsp;&nbsp;
                              <Link to="">Remove all</Link>
                            </div>
                          </h4>
                        </div> */}
                        <div></div>
                        <div class="oneColumn">
                          {/* <div class="managerRow">
                            <span>
                              <span class="managerValue">
                                <div class="react-toggle react-toggle--checked">
                                  <div class="react-toggle-track">
                                    <div class="react-toggle-track-check">
                                      <svg
                                        width="14"
                                        height="11"
                                        viewBox="0 0 14 11"
                                      >
                                        <title>switch-check</title>
                                      </svg>
                                    </div>
                                    <div class="react-toggle-track-x">
                                      <svg
                                        width="10"
                                        height="10"
                                        viewBox="0 0 10 10"
                                      >
                                        <title>switch-x</title>
                                      </svg>
                                    </div>
                                  </div>
                                  <div class="react-toggle-thumb"></div>
                                  <input
                                    type="checkbox"
                                    class="react-toggle-screenreader-only"
                                    value="on"
                                  />
                                </div>
                              </span>
                              <span class="employeeName trimmedName">
                                Administration ● Amazon
                              </span>
                            </span>
                            <span>
                              <div>
                                <span>
                                  <span>15$/hr </span>
                                  <Link to="" title="Visible to this employee">
                                    <span class="change">change</span>
                                    <span class="change">
                                      Set project pay rate
                                    </span>
                                  </Link>
                                </span>
                                <span class="rate-edit">
                                  <input type="number" step="any" value="" />
                                  &nbsp;$/hr
                                </span>
                              </div>
                            </span>
                          </div> */}
                          <div class="managerRow">
                            {/* <span>
                              <span class="managerValue">
                                <div class="react-toggle react-toggle--checked">
                                  <div class="react-toggle-track">
                                    <div class="react-toggle-track-check">
                                      <svg
                                        width="14"
                                        height="11"
                                        viewBox="0 0 14 11"
                                      >
                                        <title>switch-check</title>
                                      </svg>
                                    </div>
                                    <div class="react-toggle-track-x">
                                      <svg
                                        width="10"
                                        height="10"
                                        viewBox="0 0 10 10"
                                      >
                                        <title>switch-x</title>
                                      </svg>
                                    </div>
                                  </div>
                                  <div class="react-toggle-thumb"></div>
                                  <input
                                    type="checkbox"
                                    class="react-toggle-screenreader-only"
                                    value="on"
                                  />
                                </div>
                              </span>
                              <span class="employeeName trimmedName">
                                Development ● Microsoft
                              </span>
                            </span> */}
                            {/* <span>
                              <div>
                                <span>
                                  <span>20$/hr </span>
                                  <Link to="" title="Visible to this employee">
                                    <span class="change">change</span>
                                    <span class="change">
                                      Set project pay rate
                                    </span>
                                  </Link>
                                </span>
                                <span class="rate-edit">
                                  <input type="number" step="any" value="" />
                                  &nbsp;$/hr
                                </span>
                              </div>
                            </span> */}
                          </div>
                        </div>
                      </div>
                      <div class="employment-settings">
                        <h4 class="employment-settings__header">
                          Effective settings
                        </h4>
                        <div class="employment-settings__setting">
                          <span class="employment-settings__description">
                          screentime
                          </span>
                          <span class="employment-settings__value">
                            <Link to="">
                              12/hr, allow blur
                            </Link>
                          </span>
                        </div>
                        <div class="employment-settings__setting">
                          <span class="employment-settings__description">
                            Activity Level tracking
                          </span>
                          <span class="employment-settings__value">
                            <Link to="">Track</Link>
                          </span>
                        </div>
                        <div class="employment-settings__setting">
                          <span class="employment-settings__description">
                            App &amp; URL tracking
                          </span>
                          <span class="employment-settings__value">
                            <Link to="">Track</Link>
                          </span>
                        </div>
                        <div class="employment-settings__setting">
                          <span class="employment-settings__description">
                            Weekly time limit
                          </span>
                          <span class="employment-settings__value">
                            <Link to="">100 hours</Link>
                          </span>
                        </div>
                        <div class="employment-settings__setting">
                          <span class="employment-settings__description">
                            Auto-pause tracking after
                          </span>
                          <span class="employment-settings__value">
                            <Link to="">5 minutes</Link>
                          </span>
                        </div>
                        <div class="employment-settings__setting">
                          <span class="employment-settings__description">
                            Allow adding Offline Time
                          </span>
                          <span class="employment-settings__value">
                            <Link to="">Allow</Link>
                          </span>
                        </div>
                        <div class="employment-settings__setting">
                          <span class="employment-settings__description">
                            Notify when screentime is taken
                          </span>
                          <span class="employment-settings__value">
                            <Link to="">
                              Notify
                            </Link>
                          </span>
                        </div>
                      </div>
                      <div>Sending invitation email...</div>
                    </div>
                  </div>
                  <div id="txt">
                    <i class="fa fa-info-circle" id="ic"></i>Employees can see their own
                    rates, but not the rates of others. You will not be billed
                    for archived users. You will not be billed for owners unless
                    they track their own time. Archived users will not be able
                    to track time for your company and you will not be charged
                    for them.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
