import React from "react";
import {Link} from 'react-router-dom'
import './download.css';
export default function Download() {
  return (
    <div>
      <div id="content_wrapper">
        <div id="content_wrapper_inner">
          <div id="react-view">
            <div>
              <div id="page_title" class="smaller">
                <h1>Download application</h1>
              </div>
              <div id="content_wide" class="tpl-download" style={{padding:'2%'}}>
                <h4>Download employee desktop application</h4>
                <div id="tplImg">
                  <img id="tpl-Handel-img" src="https://screenshotmonitor.com/content/img/tour/30-status.png" />
                </div>
                <p>
                  This lightweight application is{" "}
                  <b>only for employees, not managers</b>.<br></br>
                  Company managers can see the recorded time and screentime on
                  this website.
                </p>
                <p style={{textAlign:'center'}}>
                  <Link  class="action_micro" id="downloadImg">
                    <i class="fa fa-download"></i>&nbsp;&nbsp; Download
                    application for Windows
                  </Link>
                  <br></br>
                  <Link  id="macOS">Download application for macOS</Link>
                  <br></br>
                  <Link  id="linux">Download application for Linux</Link>
                  <br></br>
                  <Link to="/" id="linux">Download browser extension</Link>
                </p>
                <h4>What is this?</h4>
                <p>
                  This is a Windows desktop application for employees. It is
                  started and stopped by an employee to track time and take
                  their computer screentime during work.
                </p>
                <p>
                  After the Stop button is pressed – no screentime are being
                  taken. You can review your time and screentime at
                  <Link >My Home</Link>. You can also delete your screentime
                  there.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
