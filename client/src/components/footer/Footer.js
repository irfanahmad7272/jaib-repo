import React from 'react';
import {Link} from 'react-router-dom'
import './footer.css';
export default function Footer() {
    return (
        <div>
          <div id="site_footer_wrapper">
    <div id="site_footer">
        <ul id="footer_left" style={{display:'flex'}}>
            <li style={{marginTop:11}}><Link id="font" >Support</Link></li>
            <li style={{marginTop:11}}><Link id="font" >Blog</Link></li>
            <li style={{marginTop:11}}><Link id="font" >About us</Link></li>
            <li style={{marginTop:11}}><Link id="font" >Contact</Link></li>
            <li style={{marginTop:11}}><Link id="font" >Affiliate program</Link></li>
            <li style={{marginTop:11}}><Link id="font" >Terms</Link></li>
            <li style={{marginTop:11}}><Link id="font" >Privacy</Link></li>
<li>
    <ul style={{display:'flex', padding:0}} id="iocon">

        <li >
            <Link  style={{padding:10}} >
                <i class="fa fa-facebook-square color-facebook"></i>
            </Link>
        </li>
        <li >
            <Link  target="_blank" style={{padding:10}}>
                <i class="fa fa-twitter-square color-twitter"></i>
            </Link>
        </li>
        
        <li >
            <Link  target="_blank" style={{padding:10}}>
                <i class="fa fa-linkedin-square color-linkedin"></i>
            </Link>
        </li>

    </ul>
</li>        </ul>
      
    </div>
        <div class="copyright">
            © 2020 Screentime Monitor
        </div>
</div>
  
        </div>
    )
}
