import React , {useState , useEffect} from "react";
import { Link , Redirect } from "react-router-dom";
  import './login.css';
  import {connect} from 'react-redux'
import { login } from "../../redux/actions/authAction";
 function Login(props) {
  const [formData, setFormData]  = useState({
    email:"mujahid@gmail.com",
    password:"password"
  })
  useEffect(()=>{
    console.log('props.auth',props.auth)
  })
     // Redirect
     if (props.auth != undefined && props.auth.isAuthenticated === true  )  {
      return <Redirect to="/" />
    }

  const handleInputChange = event => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };
const submitForm = (e)=>{
  e.preventDefault();
  props.login(formData)
}
  return (
    <div id="page_signup">
      <div id="page_inner">
        <div id="content_wrapper">
          <div id="content_wrapper_inner">
            <div id="content_wide" class="static">
              <div id="login-container">
                <h2 class="fancyserif" style={{    color: '#db2b0b'}}>Log In</h2>

                <form class="classic">
                  <fieldset>
                    <div>
                      <label id="label-login" for="Email">Email</label>
                      <input id="Email" name="email" type="text" value={formData.email}   onChange={(e)=>handleInputChange(e)} />
                    </div>
                    <div>
                      <label id="label-login" for="Password">Password</label>
                      <input id="Password" name="password" type="password" value={formData.password}   onChange={(e)=>handleInputChange(e)}/>
                    </div>
                    <div class="remember">
                      <label id="label-login" for="Remember">Remember me</label>
                      <input
                        checked="checked"
                        id="Remember"
                        name="Remember"
                        type="checkbox"
                        value="true"
                      />
                      <input name="Remember" type="hidden" value="false" />
                    </div>
                    <div class="submit">
                      <input
                        type="submit"
                        class="action_main signin"
                        value="Log In"
                        onClick={(e)=>submitForm(e)}
                      />
                      <p class="forgotten">
                       
                      </p>
                    </div>
                  </fieldset>
                </form>
                <br></br>
                  <br></br>
                    <h2 class="fancyserif" style={{color: '#db2b0b'}}>Don't have an account? </h2>
                    <span id="signup-link">
                      <Link to="">Sign up</Link> to get started!
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
  );
}
const mapStateToProps = state => ({
  auth: state.auth
});
const mapDispatchToProps = dispatch =>{
  return {
    login : data => dispatch(login(data))
  }
}
export default connect(mapStateToProps , mapDispatchToProps)(Login);