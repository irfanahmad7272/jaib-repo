import React from "react";
import "./support.css";
import SecondFooter from "./SecondFooter";
import {Link} from 'react-router-dom'
export default function Support() {
  return (
    <div id="page" class="hfeed site">
      {/* header start  */}
      <header id="masthead" class="site-header" role="banner">
        <Link class="home-link" to="" title="screentime Monitor Blog" rel="home">
          <h1 class="site-title">screentime Monitor Blog</h1>
          <h2 class="site-description">
            Employee time tracking and screentime monitoring made simple
          </h2>
        </Link>
      </header>
      {/* header end  */}
      <div id="main" class="site-main">
        <div id="primary" class="content-area">
          <div id="content" class="site-content" role="main">
            <header class="archive-header">
              <h1 class="archive-title">Category Archives: Support</h1>
            </header>
            {/* artical first  */}
            <article
              id="post-1242"
              class="post-1242 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link to="" rel="bookmark">
                    Troubleshooting
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      to=""
                      title="Permalink to Troubleshooting"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2018-10-25T10:34:54+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link to="" rel="category tag">
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link to="" rel="tag">
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      to=""
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">How to send Error Report (Windows)</h4>
                <p>
                  If you encounter errors with the program. Click on the menu
                  icon and select “Send error report…” item.<br></br>
                  Type brief description of the issue.<br></br>
                  Click Send. The report will include program log and your
                  email.{" "}
                  <Link to="" class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link to="">
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artical first end  */}
            {/* artical 2nd start  */}
            {/* <article
              id="post-1240"
              class="post-1240 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link to="" rel="bookmark">
                    Plans &amp; Pricing
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      to=""
                      title="Permalink to Plans &amp; Pricing"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2018-10-25T10:13:40+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link
                      to="https://screenshotmonitor.com/blog/category/support/"
                      rel="category tag"
                    >
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link
                      to="https://screenshotmonitor.com/blog/tag/support/"
                      rel="tag"
                    >
                      Support
                    </a>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      to="https://screenshotmonitor.com/blog/author/rodion/"
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">For how many users do I have to pay?</h4>
                <article dir="ltr" id="post-1122">
                  <p class="intercom-align-left">
                    You will be charged for all team members on your Team page
                    except archived. Account owners are free unless they track
                    their own time. For example you have 6 people on your Team:
                    5 employees (1 of them archived) and yourself – you will be
                    charged for 4 people assuming you do not track your own
                    time.
                  </p>
                  <p class="intercom-align-left">
                    The fees are prorated – if you add an employee for a week,
                    then remove or archive them – you’ll pay for that week only,
                    not month. You are free to add or delete employees, archive
                    or restore them any time as you see fit.
                  </p>
                  <p>
                    {" "}
                    <Link to="" class="more-link">
                      Continue reading <span class="meta-nav">→</span>
                    </Link>
                  </p>
                </article>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link to="">
                    <span class="leave-reply">Leave a comment</span>
                  </a>{" "}
                </div>
              </footer>
            </article> */}
            {/* artical second  end  */}
            {/* artical 34d start  */}
            <article
              id="post-1240"
              class="post-1240 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link
                    to="https://screenshotmonitor.com/blog/plans-pricing/"
                    rel="bookmark"
                  >
                    Plans &amp; Pricing
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      to="https://screenshotmonitor.com/blog/plans-pricing/"
                      title="Permalink to Plans &amp; Pricing"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2018-10-25T10:13:40+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link
                      to="https://screenshotmonitor.com/blog/category/support/"
                      rel="category tag"
                    >
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link
                      to="https://screenshotmonitor.com/blog/tag/support/"
                      rel="tag"
                    >
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      to="https://screenshotmonitor.com/blog/author/rodion/"
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">For how many users do I have to pay?</h4>
                <article dir="ltr">
                  <p class="intercom-align-left">
                    You will be charged for all team members on your Team page
                    except archived. Account owners are free unless they track
                    their own time. For example you have 6 people on your Team:
                    5 employees (1 of them archived) and yourself – you will be
                    charged for 4 people assuming you do not track your own
                    time.
                  </p>
                  <p class="intercom-align-left">
                    The fees are prorated – if you add an employee for a week,
                    then remove or archive them – you’ll pay for that week only,
                    not month. You are free to add or delete employees, archive
                    or restore them any time as you see fit.
                  </p>
                  <p>
                    {" "}
                    <Link
                      to="https://screenshotmonitor.com/blog/plans-pricing/#more-1240"
                      class="more-link"
                    >
                      Continue reading <span class="meta-nav">→</span>
                    </Link>
                  </p>
                </article>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link to="https://screenshotmonitor.com/blog/plans-pricing/#respond">
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artcial 3rd end  */}
            {/* artical fourth start  */}
            <article
              id="post-1238"
              class="post-1238 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link to="" rel="bookmark">
                    How to
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link to=" title=" Permalink to How rel="bookmark">
                      <time
                        class="entry-date"
                        datetime="2018-10-25T10:08:07+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link to="" rel="category tag">
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link to="" rel="tag">
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      to=""
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">How to add employee?</h4>
                <p>
                  Assuming you’ve started the free trial – go to your{" "}
                  <Link to="" target="_blank" rel="nofollow noopener noreferrer">
                    Team page
                  </Link>
                  , type the email into “Add new employee by email” box and
                  press Invite button.<br></br>
                  The new employee would receive an email asking them to sign in
                  into{" "}
                  <Link to="" target="_blank" rel="nofollow noopener noreferrer">
                    Screentime Monitor.com
                  </Link>{" "}
                  with the email you’ve typed. Once they do – they become a part
                  of your team and are able to track time for your company.
                  <br></br>
                  What if an employee has not received an email? There’s no need
                  to “invite again” or resend the email – that email is
                  optional. What important is the email address. Just ask the
                  employee to{" "}
                  <Link to="" target="_blank" rel="nofollow noopener noreferrer">
                    sign up
                  </Link>{" "}
                  with that email and you will see them on your team.{" "}
                  <Link to="" class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link to="">
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artical fourth end  */}
            {/* artical five start  */}
            <article
              id="post-1236"
              class="post-1236 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link to="" rel="bookmark">
                    Getting Started
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      to=""
                      title="Permalink to Getting Started"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2018-10-25T09:59:55+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link to="" rel="category tag">
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link to="" rel="tag">
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      to=""
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">
                  Explain Screentime Monitor in a few words please
                </h4>
                <p>
                  Screentime Monitor is a time tracking / screentime monitoring
                  tool used by freelancers, remote and in-office employees to
                  get a clear picture of time and money spent on each project,
                  client and task. Employees start monitoring using a
                  lightweight desktop application. The tracked time,
                  screentime, activity level, active applications &amp; URLs,
                  selected projects and task notes – are all uploaded online for
                  both manager and employee to see. See{" "}
                  <Link to="" target="_blank" rel="nofollow noopener noreferrer">
                    How it works
                  </Link>{" "}
                  for more details.{" "}
                  <Link to="" class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link to="">
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artical five end  */}
            {/* artical six start  */}
            <article
              id="post-1230"
              class="post-1230 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link to="" rel="bookmark">
                    How it works
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link to="" title="Permalink to How it works" rel="bookmark">
                      <time
                        class="entry-date"
                        datetime="2018-10-25T09:44:43+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link to="" rel="category tag">
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link to="" rel="tag">
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      to=""
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">How to automate reports?</h4>
                <p>
                  On{" "}
                  <Link to="" target="_blank" rel="nofollow noopener noreferrer">
                    Reports page
                  </Link>{" "}
                  you have an option to run reports for any number of users,
                  &nbsp;save it, download PDF, Excel and email reports on
                  schedule.{" "}
                  <Link to="" class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link to="">
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artical six end  */}
          </div>
        </div>

        <div id="tertiary" class="sidebar-container" role="complementary">
          <div class="sidebar-inner">
            <div class="widget-area">
               <aside
                id="simple-social-icons-2"
                class="widget simple-social-icons"
              >
                <h3 class="widget-title">Follow Us</h3>
                <ul class="alignleft" style={{display:'flex', marginLeft:30}}>
                  <li class="ssi-facebook">
                    <Link
                      to=""
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                   <i style={{fontSize:28 , padding:15}} class="fa fa-facebook"></i>
                    </Link>
                  </li>
                  <li class="ssi-gplus">
                    <Link
                      to=""
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                    <i style={{fontSize:28 , padding:15}} class="fa fa-twitter"></i>
                    </Link>
                  </li>
                  <li class="ssi-linkedin">
                    <Link
                      to=""
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i style={{fontSize:28 , padding:15}} class="fa fa-google"></i>
                    </Link>
                  </li>
                  <li class="ssi-twitter">
                    <Link
                      to=""
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i style={{fontSize:28 , padding:15}} class="fa fa-linkedin"></i>
                    </Link>
                  </li>
                </ul>
              </aside> 
              <aside id="categories-3" class="widget widget_categories">
                <h3 class="widget-title">Categories</h3>{" "}
                <ul>
                  <li class="cat-item cat-item-12">
                    <Link to="">Coding</Link>
                  </li>
                  <li class="cat-item cat-item-8">
                    <Link to="">Employee Monitoring</Link>
                  </li>
                  <li class="cat-item cat-item-38">
                    <Link to="">Freelancing</Link>
                  </li>
                  <li class="cat-item cat-item-37">
                    <Link to="">How To</Link>
                  </li>
                  <li class="cat-item cat-item-4">
                    <Link to="">Management</Link>
                  </li>
                  <li class="cat-item cat-item-11">
                    <Link to="">Open Source</Link>
                  </li>
                  <li class="cat-item cat-item-7">
                    <Link to="">Outsourcing</Link>
                  </li>
                  <li class="cat-item cat-item-3">
                    <Link to="">Productivity</Link>
                  </li>
                  <li class="cat-item cat-item-6">
                    <Link to="">screentime Monitor</Link>
                  </li>
                  <li class="cat-item cat-item-39 current-cat">
                    <Link to="">Support</Link>
                  </li>
                  <li class="cat-item cat-item-9">
                    <Link to="">Time Tracking</Link>
                  </li>
                  <li class="cat-item cat-item-1">
                    <Link to="">Uncategorized</Link>
                  </li>
                  <li class="cat-item cat-item-10">
                    <Link to="">Updates</Link>
                  </li>
                </ul>
              </aside>{" "}
              <aside id="recent-posts-2" class="widget widget_recent_entries">
                {" "}
                <h3 class="widget-title">Recent Posts</h3>{" "}
                <ul>
                  <li>
                    <Link to="">Screentime Monitor.com vs LumOffice.com</Link>
                  </li>
                  <li>
                    <Link to="">
                      Time tracking for WFH employees during the coronavirus
                      outbreak
                    </Link>
                  </li>
                  <li>
                    <Link to="">Troubleshooting</Link>
                  </li>
                  <li>
                    <Link to="">Plans &amp; Pricing</Link>
                  </li>
                  <li>
                    <Link to="">How to</Link>
                  </li>
                </ul>
              </aside>
            </div>
          </div>
        </div>
      {/* footer start  */}
    
      {/* footer end  */}
      </div>
    </div>
  );
}
