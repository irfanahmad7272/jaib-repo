import React, {useState} from 'react'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText ,
    Container, Row, Col
  } from 'reactstrap';
import logo from './assets/demo.PNG';
import img from './assets/Capture.PNG';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import img2 from './assets/Capture1.PNG';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
    button: {
      margin: theme.spacing(1),
    },
  }));
export default function Demo() {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    return (
       <div style={{backgroundColor:'#F4F8FB'}}>
      <Navbar color="light" light expand="md">
        {/* <NavbarBrand> */}
            <img src={logo}  style={{width:'20%'}} />
        {/* </NavbarBrand> */}
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink style={{color:'#20A2F3' , textDecoration:'underline'}} >Explore</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Genre dfdfdgdf</NavLink>
            </NavItem>
            <NavItem>
              <NavLink >Forum</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Search</NavLink>
            </NavItem>
          </Nav>
          <NavbarText>
          <i style={{fontSize:28}} class="fa fa-envelope"></i>
          <img style={{width:70, padding:10 , position:'relative', top:-6}} src={img} />
          </NavbarText>
        </Collapse>
      </Navbar>
      <Row>
        <Col xs="6" style={{textAlign:'center'}}>
            <h2 style={{fontWeight:'bold', borderBottom:'2px solid black', display:'inline'}}>Featured Novel</h2>
            <h3 style={{position:'relative', top:50, color:'#20a2f3'}}>"The Force between us"</h3>
        </Col>
     
        <Col xs="6" style={{textAlign:'center', }}>
        <Button
        style={{backgroundColor:'#20A2F3'}}
        variant="contained"
        color="primary"
        className={classes.button}
        endIcon={<Icon>send</Icon>}
      >
        Start Writing
      </Button>
      <div style={{marginLeft:-230}} id="test">
<img src={img2}/>
      </div>
        </Col>
      </Row>
    </div>
    )
}
