import React , {useEffect} from "react";
import { Link } from "react-router-dom";
import './profile.css';
import {connect} from 'react-redux';
import {loadUserProfile} from '../../redux/actions/ProfileAction';
 function ShowProfile(props) {
    useEffect(() => {
      
      let loaduserProfile = localStorage.getItem('token')
    //   console.log('loaduser',loaduserProfile)
       props.loadUserProfile(loaduserProfile)
      // console.log('loaduser',loaduser)
     
    },[]);
    console.log('user profile', props.auth)
  return (
    <div>
      <div id="content_wrapper">
        <div id="content_wrapper_inner">
          <div id="react-view" style={{marginLeft:20}}>
            <div id="content_wide">
              <h3>{props.auth.user} </h3>
            </div>
            <p>
            {props.auth.company}
              <br />
              {props.auth.email}
              <br />
              <span>UTC+05:00</span>
              <br />
            </p>
            <div>
              <i class="icon-vcard ii"></i>
              &nbsp;
              <Link to="">Edit profile</Link>
              &nbsp;&nbsp;
              <i class="icon-key ii"></i>
              &nbsp;
              <Link to="">Change password</Link>
              &nbsp;&nbsp;
              <i class="icon-at ii"></i>
              &nbsp;
              <Link to="">Change email</Link>
              <br />
              <i class="icon-close ii"></i>&nbsp;
              <Link to="">Delete my account</Link>
            </div>
            <br />
            <br />
            <h3>Company plan</h3>
            <p>
              If you track your time for other companies - you do not need a
              plan and do not have to pay - your company pays for you.
            </p>
            <br />
            <br />
            <h3 name="apidoc">ScreenshotMonitor API v2 (beta)</h3>
            <p>
              ScreenshotMonitor provides complete RESTful API to read and manage
              all your data.
            </p>
            <p>
              Every request should have <code>X-SSM-Token</code> HTTP header
              (see token value below).
            </p>
            <br />
            <br />
            <h3>ScreenshotMonitor API v1 (obsolete)</h3>
            <p>
              Use ScreenshotMonitor API to retrieve tracked time and task notes
              by employee in{" "}
              <Link to="">JSON</Link> format.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
const mapStateToProps = state => ({
    profile: state.profile,
    auth:state.auth
  });
  const mapDispatchToProps = dispatch =>{
    return {
        loadUserProfile : data => dispatch(loadUserProfile(data))
    }
  }
export default connect(mapStateToProps , mapDispatchToProps)(ShowProfile);