import React,{useEffect , useState} from "react";
import {RegisterProfile} from '../../redux/actions/ProfileAction';
import {connect} from 'react-redux';
import {toast} from 'react-toastify';
 function Profile(props) {
  const [ formData, setFormData] = useState({
    username:"",
    email:"",
    company:"",
    country:"",
    address1:"",
    address2:"",
    city:"",
    StateProvince:"",
    ZipPostalCode:""
  })
  const handleInputChange = event => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };
const submitForm = (e)=>{
  e.preventDefault();
  if(formData.username == "" ||
  formData.email == "" ||
  formData.company == "" ||
  formData.country == "" ||
  formData.address1 == "" ||
  formData.address2 == "" ||
  formData.city == "" ||
  formData.StateProvince == "" ||
  formData.ZipPostalCode == ""){
  toast.error('Input Fields Requires');
  }else{

    props.RegisterProfile(formData)
    // console.log('formData',formData)
  }
}
  return (
    <div>
      <div id="content_wrapper">
        <div id="content_wrapper_inner">
          <div id="react-view">
            <div>
              <div id="page_title" class="smaller">
                <h1>profile</h1>
              </div>

              <div style={{ width: 350, marginTop: 20, marginLeft: 25 }}>
                <label>Name</label>
                <input
                  type="text"
                  class="form-control"
                  name="username"
                  value={formData.username}
                  aria-label="Username"
                  aria-describedby="basic-addon1"
                  onChange={(e)=>handleInputChange(e)}
                  required
                  />
                <br></br>
                <label>Email</label>
                <input
                  type="text"
                  class="form-control"
                  name="email"
                  required
                  value={formData.email}
                  aria-label="email"
                  aria-describedby="basic-addon1"
                  onChange={(e)=>handleInputChange(e)}
                />
                <br></br> <label>Company</label>
                <input
                  type="text"
                  class="form-control"
                  name="company"
                  required
                  value={formData.company}
                  aria-label="company"
                  aria-describedby="basic-addon1"
                  onChange={(e)=>handleInputChange(e)}
                />
                <br></br> <label>Country</label>
                <input
                  type="text"
                  class="form-control"
                  name="country"
                  value={formData.country}
                  required
                  aria-label="country"
                  aria-describedby="basic-addon1"
                  onChange={(e)=>handleInputChange(e)}
                />
                <br></br> <label>Address Line 1</label>
                <input
                  type="text"
                  class="form-control"
                  name="address1"
                  value={formData.address1}
                  required
                  aria-label="address1"
                  aria-describedby="basic-addon1"
                  onChange={(e)=>handleInputChange(e)}
                />
                <br></br> <label>Address Line 2</label>
                <input
                  type="text"
                  class="form-control"
                  name="address2"
                  required
                  value={formData.address2}
                  aria-label="address2"
                  aria-describedby="basic-addon1"
                  onChange={(e)=>handleInputChange(e)}
                />
                <br></br> <label>City</label>
                <input
                  type="text"
                  class="form-control"
                  name="city"
                  value={formData.city}
                  required
                  aria-label="city"
                  aria-describedby="basic-addon1"
                  onChange={(e)=>handleInputChange(e)}
                />
                <br></br> <label>State/Province</label>
                <input
                  type="text"
                  class="form-control"
                  name="StateProvince"
                  required
                  value={formData.StateProvince}
                  aria-label="State-Province"
                  onChange={(e)=>handleInputChange(e)}
                  aria-describedby="basic-addon1"
                />
                <br></br> <label>Zip/Postal Code</label>
                <input
                  type="text"
                  class="form-control"
                  name="ZipPostalCode"
                  required
                  value={formData.ZipPostalCode}
                  aria-label="Zip-Postal Code"
                  aria-describedby="basic-addon1"
                  onChange={(e)=>handleInputChange(e)}
                />
                <br></br>
                <button class="btn btn-success" onClick={(e)=>submitForm(e)}>Save</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const mapStateToProps = state => ({
  auth: state.auth
});
const mapDispatchToProps = dispatch =>{
  return {
    RegisterProfile : data => dispatch(RegisterProfile(data))
  }
}
export default connect(null , mapDispatchToProps)(Profile);