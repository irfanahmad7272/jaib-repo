import React , {useEffect , useState} from "react";
import { Link } from "react-router-dom";
import "./setting.css";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import FormGroup from '@material-ui/core/FormGroup';
import Switch from '@material-ui/core/Switch';
import Screenshorts from "./screenshorts";
import { Route, BrowserRouter, Redirect } from "react-router-dom";
import ActivityLevelTracking from "./ActivityLevelTracking";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function ReportSetting() {
  const classes = useStyles();
  const [value, setValue] = React.useState("female");
  const [age, setAge] = React.useState("");
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
    checkedC: true,
    checkedD: true,
    checkedE: true,
  });
  const [tabs, setTabs]= useState({
    currentView: 'one'
  })

  const handleChangeSwitch = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  const handleChangeNum = (event) => {
    setAge(event.target.value);
  };
  const handleChange = (event) => {
    setValue(event.target.value);
  };
 
  return (
    <div id="content_wrapper">

      <div id="content_wrapper_inner">
        <div id="react-view">
          <div id="page_title">
            <h1>Settings</h1>
          </div>
          <div class="csettings">
            {/* first div  */}
            <div class="csettings__list">
              <Link
                class="csettings__list-item csettings__list-item-active"
                // to="/setting/screenshorts"
              >
                screentime
                <span class="csettings__list-item-value">12/hr</span>
              </Link>
              <Link class="csettings__list-item" to="/setting/ActivityLevelTracking" >
                Activity Level tracking{" "}
                <span class="csettings__list-item-value">Yes</span>
              </Link>
              <Link class="csettings__list-item" >
                App &amp; URL tracking{" "}
                <span class="csettings__list-item-value">Yes</span>
              </Link>
              <Link class="csettings__list-item firstInGroup" >
                Weekly time limit{" "}
                <span class="csettings__list-item-value">100 h</span>
              </Link>
              <Link class="csettings__list-item" >
                Auto-pause tracking after{" "}
                <span class="csettings__list-item-value">5 min</span>
              </Link>
              <Link class="csettings__list-item" >
                Allow adding Offline Time{" "}
                <span class="csettings__list-item-value">Yes</span>
              </Link>
              <Link class="csettings__list-item" >
                Notify when screentime is taken{" "}
                <span class="csettings__list-item-value">Yes</span>
              </Link>
              <Link class="csettings__list-item firstInGroup" >
                Week starts on{" "}
                <span class="csettings__list-item-value">Sun</span>
              </Link>
              <Link class="csettings__list-item" >
                Currency symbol{" "}
                <span class="csettings__list-item-value">$</span>
              </Link>
              <Link class="csettings__list-item" >
                Employee desktop application settings{" "}
                <span class="csettings__list-item-value"></span>
              </Link>
            </div>
            {/* first div end  */}
            {/* second div start  */}
            
             {/* <div class="csettings__detail">  */}
            
            <Screenshorts/>
           {/*  <Route exact path="/setting/screenshorts" component={Screenshorts} />
           <Route exact path="/setting/ActivityLevelTracking" component={ActivityLevelTracking} /> */}
          
            {/* </div>  */}
          
            {/* second div end  */}
          </div>
        </div>
      </div>
    </div>
  );
}
