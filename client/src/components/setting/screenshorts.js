import React from 'react'
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import FormGroup from '@material-ui/core/FormGroup';
import Switch from '@material-ui/core/Switch';
const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));
export default function Screenshorts() {
    const classes = useStyles();
    const [value, setValue] = React.useState("female");
    const [age, setAge] = React.useState("");
    const [state, setState] = React.useState({
      checkedA: true,
      checkedB: true,
      checkedC: true,
      checkedD: true,
      checkedE: true,
    });
    const handleChangeSwitch = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
      };
      const handleChangeNum = (event) => {
        setAge(event.target.value);
      };
      const handleChange = (event) => {
        setValue(event.target.value);
      };
    return (
        <div class="csettings__detail"> 
        <h3 class="csettings__detail-title">screentime</h3>
        <div class="csettings__detail-info"></div>
        <div class="csettings__detail-descr">
          How frequently screentimes will be taken. <br></br>This number
          is an average since screentime are taken at random intervals.{" "}
        </div>
        <div>
          <FormControl component="fieldset">
            <FormLabel component="legend">Gender</FormLabel>
            <RadioGroup
              aria-label="gender"
              name="gender1"
              value={value}
              onChange={handleChange}
              style={{ display: "flex", flexDirection: "row" }}
            >
              <FormControlLabel
                value="take"
                control={<Radio />}
                label="Take"
              />
              <FormControl
                className={classes.formControl}
                style={{ marginTop: -12 }}
              >
                <InputLabel id="demo-simple-select-label">Num</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={age}
                  onChange={handleChangeNum}
                >
                  <MenuItem value={3}>Three</MenuItem>
                  <MenuItem value={5}>Five</MenuItem>
                  <MenuItem value={6}>Six</MenuItem>
                  <MenuItem value={7}>Seven</MenuItem>
                  <MenuItem value={9}>Nime</MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>
              <span style={{ marginTop: 10, margin: 8 }}>
                screentime per hour
              </span>
              <FormControlLabel
                value="notTake"
                control={<Radio />}
                label="Do not take"
              />
            </RadioGroup>
          </FormControl>
          <div>
            <h4 class="csettings__individual-setting-title">
              Individual settings
            </h4>
            <div>
           
            <div class="csettings__individual-setting-descr">If enabled, the individual setting will be used instead of the team setting</div>
            <FormGroup row>
              <FormControlLabel
                control={<Switch checked={state.checkedA} onChange={handleChangeSwitch} name="checkedA" color="default"/>}
                label="Brian May"
              />
            
            </FormGroup>
            <FormGroup row>
              <FormControlLabel
                control={<Switch checked={state.checkedB} onChange={handleChangeSwitch} name="checkedB" color="default"/>}
                label="Demo Manager"
              />
            
            </FormGroup>
            <FormGroup row>
              <FormControlLabel
                control={<Switch checked={state.checkedC} onChange={handleChangeSwitch} name="checkedC" color="default"/>}
                label="James Hetfield"
              />
            
            </FormGroup>
            <FormGroup row>
              <FormControlLabel
                control={<Switch checked={state.checkedD} onChange={handleChangeSwitch} name="checkedD" color="default"/>}
                label="Till Lindemann"
              />
            
            </FormGroup>
            <FormGroup row>
              <FormControlLabel
                control={<Switch checked={state.checkedE} onChange={handleChangeSwitch} name="checkedE" color="default"/>}
                label="Viktor Zinchuk"
              />
            
            </FormGroup>
            </div>
          </div>
        </div>
      </div>
    )
}
