import React , {useEffect , useState} from "react";
import { Link } from "react-router-dom";
import "./signup.css";
import {register} from '../../redux/actions/authAction'
import {connect} from 'react-redux'
function Signup(props) {
  const [formData, setFormData]  = useState({
    username:"",
    email:"",
    password:"",
    company:"",

  })
  const handleInputChange = event => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };
const submitForm = (e)=>{
  e.preventDefault();
  props.register(formData);
}
  return (
    <div id="page_signup">
      <div id="page_inner">
        {/* inner first start   */}
        <div id="regcontainer">
          <table id="regtable">
            <tbody>
              <tr>
                <td id="regform" class="reset-form">
                  <div id="regform-fields">
                    <form  autocomplete="on" id="regform-form">
        
                      <div class="regfield">
                        <label for="Name">Your full name</label>
                        <input
                          autofocus="autofocus"
                          id="Name"
                          maxlength="50"
                          name="username"
                          onChange={(e)=>handleInputChange(e)}
                          required="required"
                          type="text"
                          value={formData.username}
                        />
                      </div>
                      <div class="regfield">
                        <div class="regnote">optional</div>
                        <label for="Company">Company or organization</label>
                        <input
                          id="Company"
                          maxlength="50"
                          name="company"
                          type="text"
                          value={formData.company}
                          onChange={(e)=>handleInputChange(e)}
                        />
                      </div>
                      <div class="regfield">
                        <label for="Email">Email</label>
                        <input
                          id="Email"
                          maxlength="250"
                          name="email"
                          required="required"
                          type="email"
                          value={formData.email}
                          onChange={(e)=>handleInputChange(e)}
                        />
                      </div>
                      <div class="regfield">
                        <div class="regnote">6 or more characters</div>
                        <label for="Password">Choose password</label>
                        <input
                          id="Password"
                          maxlength="50"
                          name="password"
                          pattern=".{6,}"
                          required="required"
                          title="The password should be at least 6 characters long"
                          type="password"
                          value={formData.password}
                          onChange={(e)=>handleInputChange(e)}
                        />
                      </div>
                      <div class="regfield">
                      <span id="h9f03d42c39f74bc69f484cd763c966ba"></span>
                      </div>
                
                      <div>
                        <input
                          type="submit"
                          value="Create account"
                          class="action_main"
                          onClick={(e)=>submitForm(e)}
                        />
                      </div>
                      <div id="pcode"></div>
                    </form>
           
                  </div>
                </td>
                <td id="texting">
                  <div id="light">
                    <h1>Try it Free for 14 days</h1>
                    <h4>and keep it Free forever on the Free Plan</h4>

                    <div id="intro_est">
                      <div>
                        <img
                          src="https://screenshotmonitor.com/Content/img/home/testimonial.png"
                          alt="Craig Crawford - Screentime-Monitor"
                        />
                      </div>
                      <i>
                        <i>
                          The best way to follow your team overseas is to
                          actually <b>see</b> what they're doing...
                        </i>
                      </i>
                      <span>- Craig Crawford, Paramount Profits</span>
                    </div>
                  </div>
                  <div id="happybus">
                    <h5>
                      <b>20 000+</b> happy businesses are using
                      Screentime Monitor
                    </h5>

                    <img src="https://screenshotmonitor.com/Content/img/home/trusted.png" />
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        {/* inner first end  */}
      </div>
    </div>
  );
}
const mapStateToProps = state => ({
  auth: state.auth
});
const mapDispatchToProps = dispatch =>{
  return {
    register : data => dispatch(register(data))
  }
}
export default connect(null , mapDispatchToProps)(Signup)