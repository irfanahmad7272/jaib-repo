import React from "react";
import {Link} from 'react-router-dom'
import "./blog.css";
export default function Blog() {
  return (
    <div id="page" class="hfeed site">
      {/* header start  */}
      <header id="masthead" class="site-header" role="banner">
        <Link class="home-link" href="" title="screentime Monitor Blog" rel="home">
          <h1 class="site-title">screentime Monitor Blog</h1>
          <h2 class="site-description">
            Employee time tracking and screentime monitoring made simple
          </h2>
        </Link>
      </header>
      {/* header end  */}
      <div id="main" class="site-main">
        <div id="primary" class="content-area">
          <div id="content" class="site-content" role="main">
            {/* first artical start  */}
            <article
              id="post-1289"
              class="post-1289 post type-post status-publish format-standard hentry category-uncategorized"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link  rel="bookmark">
                    Screentime Monitor.com vs LumOffice.com
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      
                      title="Permalink to Screentime Monitor.com vs LumOffice.com"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2020-03-24T21:26:16+00:00"
                      >
                        March 24, 2020
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link  rel="category tag">
                      Uncategorized
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      
                      title="View all posts by Ross Sudentas"
                      rel="author"
                    >
                      Ross Sudentas
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <p>
                  Both <Link >Screentime Monitor.com</Link> and{" "}
                  <Link >LumOffice.com</Link> share the following features
                </p>
                <ol>
                  <li>
                    1. Collect screentime, time worked, activity level, apps
                    used &amp; websites visited (and duration on each)
                  </li>
                  <li>
                    2. Managers can see all the collected data via browser on
                    the web
                  </li>
                  <li>3. No webcam pictures</li>
                  <li>4. No key-logging</li>
                  <li>5. screentime are taken from all monitors</li>
                  <li>
                    6. The programs are not hidden and can be uninstalled like
                    any other software
                  </li>
                  <li>7. Users should be notified about tracking</li>
                </ol>
                <p>
                  The table below summarizes the <strong>differences</strong>{" "}
                  between the two services
                </p>
                <table>
                  <tbody>
                    <tr>
                      <td>
                        <Link >Screentime Monitor.com</Link>
                      </td>
                      <td>
                        <Link >LumOffice.com</Link>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Has Start &amp; Stop tracking buttons. No tracking
                        unless started by user.
                      </td>
                      <td>Tracking is always running</td>
                    </tr>
                    <tr>
                      <td>
                        Users can add notes and select a project they work on.
                        Managers can then see times spent on each project in
                        reports
                      </td>
                      <td>Users make no notes and do not select projects</td>
                    </tr>
                    <tr>
                      <td>
                        Notifications are shown when screentime are taken
                      </td>
                      <td>No notifications when screentime are taken</td>
                    </tr>
                    <tr>
                      <td>Users have full access to tracked data</td>
                      <td>Users have no access to tracked data</td>
                    </tr>
                    <tr>
                      <td>Users can delete their screentime</td>
                      <td>Users can not delete their screentime</td>
                    </tr>
                    <tr>
                      <td>
                        Can be used to track time for multiple companies and for
                        themselves
                      </td>
                      <td>Will track time for one company only</td>
                    </tr>
                    <tr>
                      <td>Can be installed on any computer</td>
                      <td>
                        Can only be installed on company-owned computers only
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Meant for remote employees &amp; freelances where
                        company allows full control on tracking
                      </td>
                      <td>
                        Meant for office and remote employees with always-on
                        tracking
                      </td>
                    </tr>
                  </tbody>
                </table>
                <p>&nbsp;</p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link >
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* first artical end  */}
            {/* senod artical start */}
            <article
              id="post-1272"
              class="post-1272 post type-post status-publish format-standard hentry category-employee-monitoring category-screenshot-monitor category-time-tracking"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link
                    
                    rel="bookmark"
                  >
                    Time tracking for WFH employees during the coronavirus
                    outbreak
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      
                      title="Permalink to Time tracking for WFH employees during the coronavirus outbreak"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2020-03-17T12:17:43+00:00"
                      >
                        March 17, 2020
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link
                      
                      rel="category tag"
                    >
                      Employee Monitoring
                    </Link>
                    ,{" "}
                    <Link
                      
                      rel="category tag"
                    >
                      screentime Monitor
                    </Link>
                    ,{" "}
                    <Link
                      href=""
                      rel="category tag"
                    >
                      Time Tracking
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                    
                      title="View all posts by Ross Sudentas"
                      rel="author"
                    >
                      Ross Sudentas
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <p>
                  <span>
                    Because of the coronavirus&nbsp;outbreak, many employees
                    will be working from home instead of the office. Therefore,
                    it may be more difficult for many companies to stay
                    profitable during this COVID-19 pandemic. How can managers
                    ensure that employees stay productive when they
                    telecommute?&nbsp;
                  </span>
                </p>
                <h2>
                  <span>
                    How to keep productivity when employees work from home
                  </span>
                </h2>
                <p>
                  <span>
                    In some jobs, employee productivity can be measured based on
                    results such as articles written or items sold. In these
                    cases, it doesn’t matter if employees work from home or the
                    office – they are paid for results. But for many employees,
                    such as office clerks, customer service representatives,
                    managers, bookkeepers and many others –&nbsp; there are
                    often no simple criteria to know if employees are being
                    productive or if they are doing something else while being
                    paid for their time. One of the ways to ensure productivity
                    in such cases is to monitor employees. Because of the
                    coronavirus, one of the most popular time tracking and
                    screentime monitoring services,{" "}
                    <Link >
                      Screentime Monitor.com
                    </Link>{" "}
                    is now{" "}
                    <Link >
                      offering their top-tier service free until June 2020
                    </Link>{" "}
                    with an unlimited number of users.
                  </span>
                </p>
                <h2>
                  <span>How screentime Monitor works for WFH employees</span>
                </h2>
                <p>
                  <span>
                    Employees install an app on their computers and start time
                    tracking when they start working and stop when they are
                    done. A manager can open a browser and see what their
                    employees were working on, their activity level and periodic
                    screentime of their monitors.{" "}
                  </span>
                </p>
                <p>
                  <img
                    class="alignnone size-full wp-image-1276"
                    style={{ width: 800 }}
                    src="https://screenshotmonitor.com/blog/wp-content/uploads/2020/03/ssm.png"
                    alt="screentimemonitor"
                    id="handelBlogImg"
                  />
                </p>
                <p>
                  <span>
                    Also recorded are all of the programs their employees ran
                    and websites they visited during time tracking
                  </span>
                </p>
                <p>
                  <img
                    class="alignnone size-full wp-image-1277"
                    style={{ width: 800 }}
                    src="https://screenshotmonitor.com/blog/wp-content/uploads/2020/03/ssm-apps.png"
                    alt=""
                    id="handelBlogImg"
                  />
                </p>
                <p>
                  <span>
                    All of this gives managers a clear picture of where the time
                    is spent. And for many employees, just the fact that their
                    work could be reviewed may encourage them to focus more on
                    their tasks.
                  </span>
                </p>
                <h2>
                  <span>
                    How to overcome objections to screentime monitoring when
                    working from home
                  </span>
                </h2>
                <p>
                  Ideally, your employees will realize that screentime
                  monitoring can be beneficial to all involved. However, you may
                  initially encounter some resistance to this concept. Here are
                  some answers to common objections:
                </p>
                <h4>
                  <span>It is not fair</span>
                </h4>
                <p>
                  <span>
                    When employees get paid for their time they exchange their
                    time for a payment. It’s not unfair when an employer wants
                    clarification on what they are paying for.
                  </span>
                </p>
                <h4>
                  <span>It invades my privacy</span>
                </h4>
                <p>
                  <span>
                    The employees choose when to start and when to stop
                    monitoring. Anything private that appears on an employee’s
                    monitor can be deleted manually on the website. &nbsp;{" "}
                  </span>
                </p>
                <h4>
                  <span>
                    If you trust your employees – you won’t be using monitoring
                  </span>
                </h4>
                <p>
                  “Trust, but verify” is a useful phrase in this context. Assure
                  your employees that you do trust them, but that a level of
                  verification is needed in order for you not to be completely
                  blind as to what employees are working on.
                </p>
                <h4>
                  <span>My boss will want me to work 100% of time</span>
                </h4>
                <p>
                  Establish early on that you understand that your employees
                  will need the same sort of breaks that they have in the
                  office, such as getting coffee or going to the bathroom.
                  Research suggests that employees are more productive when
                  taking occasional brief breaks, so you are encouraged to keep
                  this in mind when determining what percentage of productivity
                  you expect from them.
                </p>
                <h2>
                  <span>Summary</span>
                </h2>
                <p>
                  <span>
                    Even if the coronavirus forced your company employees work
                    from home – it is not a reason for their productivity to go
                    down. Quite the opposite, with the right tools they may be
                    more productive working from home and happier without a
                    commute. And management can have a clearer picture where the
                    resources are spent while saving money on keeping an office
                    space.{" "}
                  </span>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link >
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* second artical end  */}
            {/* artical first  */}
            <article
              id="post-1242"
              class="post-1242 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link  rel="bookmark">
                    Troubleshooting
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      
                      title="Permalink to Troubleshooting"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2018-10-25T10:34:54+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link href="" rel="category tag">
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link  rel="tag">
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">How to send Error Report (Windows)</h4>
                <p>
                  If you encounter errors with the program. Click on the menu
                  icon and select “Send error report…” item.<br></br>
                  Type brief description of the issue.<br></br>
                  Click Send. The report will include program log and your
                  email.{" "}
                  <Link  class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link >
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artical first end  */}
            {/* artical 34d start  */}
            <article
              id="post-1240"
              class="post-1240 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link
                    
                    rel="bookmark"
                  >
                    Plans &amp; Pricing
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      href=""
                      title="Permalink to Plans &amp; Pricing"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2018-10-25T10:13:40+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link
                      
                      rel="category tag"
                    >
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link
                      href=""
                      rel="tag"
                    >
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">For how many users do I have to pay?</h4>
                <article dir="ltr">
                  <p class="intercom-align-left">
                    You will be charged for all team members on your Team page
                    except archived. Account owners are free unless they track
                    their own time. For example you have 6 people on your Team:
                    5 employees (1 of them archived) and yourself – you will be
                    charged for 4 people assuming you do not track your own
                    time.
                  </p>
                  <p class="intercom-align-left">
                    The fees are prorated – if you add an employee for a week,
                    then remove or archive them – you’ll pay for that week only,
                    not month. You are free to add or delete employees, archive
                    or restore them any time as you see fit.
                  </p>
                  <p>
                    {" "}
                    <Link
                      
                      class="more-link"
                    >
                      Continue reading <span class="meta-nav">→</span>
                    </Link>
                  </p>
                </article>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link href="">
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artcial 3rd end  */}
            {/* artical fourth start  */}
            <article
              id="post-1238"
              class="post-1238 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link  rel="bookmark">
                    How to
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link to=" title=" Permalink to How rel="bookmark">
                      <time
                        class="entry-date"
                        datetime="2018-10-25T10:08:07+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link  rel="category tag">
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link  rel="tag">
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">How to add employee?</h4>
                <p>
                  Assuming you’ve started the free trial – go to your{" "}
                  <Link  target="_blank" rel="nofollow noopener noreferrer">
                    Team page
                  </Link>
                  , type the email into “Add new employee by email” box and
                  press Invite button.<br></br>
                  The new employee would receive an email asking them to sign in
                  into{" "}
                  <Link  target="_blank" rel="nofollow noopener noreferrer">
                    Screentime Monitor.com
                  </Link>{" "}
                  with the email you’ve typed. Once they do – they become a part
                  of your team and are able to track time for your company.
                  <br></br>
                  What if an employee has not received an email? There’s no need
                  to “invite again” or resend the email – that email is
                  optional. What important is the email address. Just ask the
                  employee to{" "}
                  <Link  target="_blank" rel="nofollow noopener noreferrer">
                    sign up
                  </Link>{" "}
                  with that email and you will see them on your team.{" "}
                  <Link  class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link >
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artical fourth end  */}
            {/* artical five start  */}
            <article
              id="post-1236"
              class="post-1236 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link  rel="bookmark">
                    Getting Started
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      
                      title="Permalink to Getting Started"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2018-10-25T09:59:55+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link  rel="category tag">
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link  rel="tag">
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">
                  Explain Screentime Monitor in a few words please
                </h4>
                <p>
                  Screentime Monitor is a time tracking / screentime monitoring
                  tool used by freelancers, remote and in-office employees to
                  get a clear picture of time and money spent on each project,
                  client and task. Employees start monitoring using a
                  lightweight desktop application. The tracked time,
                  screentime, activity level, active applications &amp; URLs,
                  selected projects and task notes – are all uploaded online for
                  both manager and employee to see. See{" "}
                  <Link  target="_blank" rel="nofollow noopener noreferrer">
                    How it works
                  </Link>{" "}
                  for more details.{" "}
                  <Link  class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link >
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artical five end  */}
            {/* artical six start  */}
            <article
              id="post-1230"
              class="post-1230 post type-post status-publish format-standard hentry category-support tag-support"
            >
              <header class="entry-header">
                <h1 class="entry-title">
                  <Link  rel="bookmark">
                    How it works
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link  title="Permalink to How it works" rel="bookmark">
                      <time
                        class="entry-date"
                        datetime="2018-10-25T09:44:43+00:00"
                      >
                        October 25, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link  rel="category tag">
                      Support
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link  rel="tag">
                      Support
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <h4 class="t__h1">How to automate reports?</h4>
                <p>
                  On{" "}
                  <Link  target="_blank" rel="nofollow noopener noreferrer">
                    Reports page
                  </Link>{" "}
                  you have an option to run reports for any number of users,
                  &nbsp;save it, download PDF, Excel and email reports on
                  schedule.{" "}
                  <Link  class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link >
                    <span class="leave-reply">Leave a comment</span>
                  </Link>
                </div>
              </footer>
              <header class="entry-header">
                <div class="entry-thumbnail">
                  <img
                    id="handelBlogImg"
                  
                    src="https://screenshotmonitor.com/blog/wp-content/uploads/2018/10/Снимок-экрана-2018-10-04-в-21.09.54.png"
                    class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                    alt="5 Jobs with the Highest Risk of Being Substituted by Robots and Apps"
                  />{" "}
                </div>

                <h1 class="entry-title">
                  <Link
                    
                    rel="bookmark"
                  >
                    5 Jobs with the Highest Risk of Being Substituted by Robots
                    and Apps
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      
                      title="Permalink to 5 Jobs with the Highest Risk of Being Substituted by Robots and Apps"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2018-10-04T14:31:26+00:00"
                      >
                        October 4, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link href="" rel="category tag">
                      Uncategorized
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>
              <p>
                <i>
                  <span>
                    Robots are overtaking more and more jobs previously done by
                    humans. Some call it progress, some express reasonable
                    concern. To help you decide which side you’re on, here’s a
                    list of jobs soon to be substituted by robots and apps.
                  </span>
                </i>
              </p>
            </article>
            {/* artical six end  */}
            {/* artical server start  */}
            <article
              id="post-1213"
              class="post-1213 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized"
            >
              <header class="entry-header">
                <div class="entry-thumbnail" style={{ textAlign: "left" }}>
                  <img
                     id="handelBlogImg"
                    src="https://screenshotmonitor.com/blog/wp-content/uploads/2018/09/E-commerce-and-time-tracking.png"
                    class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                    alt="E-commerce and time tracking"
                  />{" "}
                </div>

                <h1 class="entry-title">
                  <Link  rel="bookmark">
                    How Can Your E-commerce Business Benefit from Time-Tracking
                  </Link>
                </h1>

                <div class="entry-meta"></div>
                <span class="date">
                  <Link
                    
                    title="Permalink to How Can Your E-commerce Business Benefit from Time-Tracking"
                    rel="bookmark"
                  >
                    <time
                      class="entry-date"
                      datetime="2018-09-23T15:12:54+00:00"
                    >
                      September 23, 2018
                    </time>
                  </Link>
                </span>
                <span class="categories-links">
                  <Link  rel="category tag">
                    Uncategorized
                  </Link>
                </span>
                <span class="author vcard">
                  <Link
                    class="url fn n"
                    
                    title="View all posts by Rodion Nikolaev"
                    rel="author"
                  >
                    Rodion Nikolaev
                  </Link>
                </span>
              </header>

              <div class="entry-content">
                <p>
                  <i>
                    <span>
                      There are clear benefits of shifting your business to the
                      online world and each year people are getting more and
                      more eager to get a piece of the ever-growing e-commerce
                      cake. In the last decade, the online retail market has
                      been growing steadily, and in the US alone the increase{" "}
                    </span>
                  </i>
                  <Link >
                    <i>
                      <span>
                        peaked at 16% in 2017, which is its highest growth rate
                        since 2011.
                      </span>
                    </i>
                  </Link>
                </p>
                <p>
                  <span>
                    The greater interest in e-commerce has lead to a constant
                    rise in competition. The result is an inevitable
                    segmentation of the market, meaning new companies are
                    building their business strategies around servicing specific
                    niches. This has allowed smaller companies to remain
                    relevant and competitive in the midst of an age where giants
                    like Amazon rule the corporate world. In order to grow their
                    business, entrepreneurs need to focus not only on their
                    consumers, but they also have to look inward and improve the
                    manner in which they organize and conduct their day-to-day
                    business.
                  </span>
                </p>
                <p>
                  {" "}
                  <Link  class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link >
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artical seven end  */}
            {/* artical eight start  */}
            <article
              id="post-1205"
              class="post-1205 post type-post status-publish format-standard has-post-thumbnail hentry category-how-to tag-freelancer tag-freelancing tag-upwork"
            >
              <header class="entry-header">
                <div class="entry-thumbnail">
                  <img
                      id="handelBlogImg"
                    src="https://screenshotmonitor.com/blog/wp-content/uploads/2018/09/up.png"
                    class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                    alt="How to optimize your Upwork profile"
                  />{" "}
                </div>

                <h1 class="entry-title">
                  <Link  rel="bookmark">
                    Upwork Profile Optimization Tips and Tricks: How to Create a
                    Client-Winning CV
                  </Link>
                </h1>

                <div class="entry-meta">
                  <span class="date">
                    <Link
                      
                      title="Permalink to Upwork Profile Optimization Tips and Tricks: How to Create a Client-Winning CV"
                      rel="bookmark"
                    >
                      <time
                        class="entry-date"
                        datetime="2018-09-10T11:37:09+00:00"
                      >
                        September 10, 2018
                      </time>
                    </Link>
                  </span>
                  <span class="categories-links">
                    <Link
                      href=""
                      rel="category tag"
                    >
                      How To
                    </Link>
                  </span>
                  <span class="tags-links">
                    <Link
                      href=""
                      rel="tag"
                    >
                      Freelancer
                    </Link>
                    ,{" "}
                    <Link
                      href=""
                      rel="tag"
                    >
                      Freelancing
                    </Link>
                    ,{" "}
                    <Link  rel="tag">
                      Upwork
                    </Link>
                  </span>
                  <span class="author vcard">
                    <Link
                      class="url fn n"
                      
                      title="View all posts by Rodion Nikolaev"
                      rel="author"
                    >
                      Rodion Nikolaev
                    </Link>
                  </span>{" "}
                </div>
              </header>

              <div class="entry-content">
                <p>
                  <i>
                    <span >
                      Getting yourself listed on Upwork as a freelancer is easy.
                      Making your profile stand out is not. Well, not now when
                      you have this article here detailing the intricacies of
                      Upwork profile optimization.
                    </span>
                  </i>
                </p>
                <p>
                  {" "}
                  <Link  class="more-link">
                    Continue reading <span class="meta-nav">→</span>
                  </Link>
                </p>
              </div>

              <footer class="entry-meta">
                <div class="comments-link">
                  <Link >
                    <span class="leave-reply">Leave a comment</span>
                  </Link>{" "}
                </div>
              </footer>
            </article>
            {/* artical eight end  */}
          </div>
        </div>
        {/* side bar start  */}
        <div id="tertiary" class="sidebar-container" role="complementary">
          <div class="sidebar-inner">
            <div class="widget-area">
              <aside
                id="simple-social-icons-2"
                class="widget simple-social-icons"
              >
                <h3 class="widget-title">Follow Us</h3>
                <ul
                  class="alignleft"
                  style={{ display: "flex", marginLeft: 30 }}
                >
                  <li class="ssi-facebook">
                    <Link  target="_blank" rel="noopener noreferrer">
                      <i
                        style={{ fontSize: 28, padding: 15 }}
                        class="fa fa-facebook"
                      ></i>
                    </Link>
                  </li>
                  <li class="ssi-gplus">
                    <Link  target="_blank" rel="noopener noreferrer">
                      <i
                        style={{ fontSize: 28, padding: 15 }}
                        class="fa fa-twitter"
                      ></i>
                    </Link>
                  </li>
                  <li class="ssi-linkedin">
                    <Link  target="_blank" rel="noopener noreferrer">
                      <i
                        style={{ fontSize: 28, padding: 15 }}
                        class="fa fa-google"
                      ></i>
                    </Link>
                  </li>
                  <li class="ssi-twitter">
                    <Link  target="_blank" rel="noopener noreferrer">
                      <i
                        style={{ fontSize: 28, padding: 15 }}
                        class="fa fa-linkedin"
                      ></i>
                    </Link>
                  </li>
                </ul>
              </aside>
              <aside id="categories-3" class="widget widget_categories">
                <h3 class="widget-title">Categories</h3>{" "}
                <ul>
                  <li class="cat-item cat-item-12">
                    <Link >Coding</Link>
                  </li>
                  <li class="cat-item cat-item-8">
                    <Link >Employee Monitoring</Link>
                  </li>
                  <li class="cat-item cat-item-38">
                    <Link >Freelancing</Link>
                  </li>
                  <li class="cat-item cat-item-37">
                    <Link >How To</Link>
                  </li>
                  <li class="cat-item cat-item-4">
                    <Link >Management</Link>
                  </li>
                  <li class="cat-item cat-item-11">
                    <Link >Open Source</Link>
                  </li>
                  <li class="cat-item cat-item-7">
                    <Link >Outsourcing</Link>
                  </li>
                  <li class="cat-item cat-item-3">
                    <Link >Productivity</Link>
                  </li>
                  <li class="cat-item cat-item-6">
                    <Link >screentime Monitor</Link>
                  </li>
                  <li class="cat-item cat-item-39 current-cat">
                    <Link >Support</Link>
                  </li>
                  <li class="cat-item cat-item-9">
                    <Link >Time Tracking</Link>
                  </li>
                  <li class="cat-item cat-item-1">
                    <Link >Uncategorized</Link>
                  </li>
                  <li class="cat-item cat-item-10">
                    <Link >Updates</Link>
                  </li>
                </ul>
              </aside>{" "}
              <aside id="recent-posts-2" class="widget widget_recent_entries">
                {" "}
                <h3 class="widget-title">Recent Posts</h3>{" "}
                <ul>
                  <li>
                    <Link >Screentime Monitor.com vs LumOffice.com</Link>
                  </li>
                  <li>
                    <Link >
                      Time tracking for WFH employees during the coronavirus
                      outbreak
                    </Link>
                  </li>
                  <li>
                    <Link >Troubleshooting</Link>
                  </li>
                  <li>
                    <Link >Plans &amp; Pricing</Link>
                  </li>
                  <li>
                    <Link >How to</Link>
                  </li>
                </ul>
              </aside>
            </div>
          </div>
        </div>
        {/* footer start  */}

        {/* footer end  */}
      </div>
    </div>
  );
}
