import React , {useEffect , usestate} from   "react";
import { Link } from "react-router-dom";
import './demoManager.css'
import Calendar from 'react-calendar';
export default function DemoManager() {
    // const [state, setState] = usestate({
    //     date:new Date(),
    // })
//    const onChangeDate = date =>{     
// setState((state)=>({
//     ...state , date
// }))
//    }
  return (
    <div id="content_wrapper">
      <div id="content_wrapper_inner">
        <div id="react-view">
          {/* //header  */}
          <div>
            <div class="timeline-header" id="page_title">
              <span class="__timezone-switch">
                All times are UTC-7
                <Link
                  to=""
                  class="fa fa-cog"
                  title=""
                  data-original-title="Change"
                >
                  <i></i>
                </Link>
              </span>
              <h1 id="handelT" >
                <span class="empl-status offline"></span>&nbsp;Demo
                Manager&nbsp;
                <Link to="">
                  <i class="fa fa-cog" title="" data-original-title="Settings">
                    <i></i>
                  </i>
                </Link>
                <span id="never_work">never worked</span>
              </h1>
            </div>
          </div>
          {/* header end  */}

          {/* body start  */}
          <div id="content_wide" class="_timeline">
            {/* calender start  */}
            <div class="_timeline_calendar">

          
        {/* <Calendar
          onChange={(e)=>onChangeDate(e)}
          value={state.date}
        /> */}
     
            </div>
            {/* calender end  */}

            {/* calender time show start  */}
           <div class="_timeline_overview">
           <div class="info-outer">
              <table class="info">
                <tbody>
                  <tr>
                    <td class="big" rowspan="1">
                      <div class="dur">0h 00m</div>
                    </td>
                    <td class="big2">
                      <div class="day">
                        Sunday, May 31
                        <span
                          title=""
                         
                          data-original-title="Average Activity Level: NaN%"
                        >
                          <i></i>
                        </span>
                      </div>
                      <div class="bot">
                        <span>
                          Week &nbsp;
                          <Link
                            to=""
                            title=""
                            data-original-title="View report"
                          >
                            <i></i>0h 00m
                          </Link>
                        </span>
                        <b>●</b>
                        <span>
                          Month &nbsp;
                          <Link
                            to=""
                            title=""
                            data-original-title="View report"
                          >
                            <i></i>0h 00m
                          </Link>
                        </span>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
           </div>
            {/* calender time show end  */}
            {/* time line hour start  */}
            <div class="_timeline_hours">
              <div class="hours">
                <div class="hours-row">
                  <div class="hour">
                    <div class="name">12am</div>
                  </div>
                  <div class="hour">
                    <div class="name">1am</div>
                  </div>
                  <div class="hour">
                    <div class="name">2am</div>
                  </div>
                  <div class="hour">
                    <div class="name">3am</div>
                  </div>
                  <div class="hour">
                    <div class="name">4am</div>
                  </div>
                  <div class="hour">
                    <div class="name">5am</div>
                  </div>
                  <div class="hour">
                    <div class="name">6am</div>
                  </div>
                  <div class="hour">
                    <div class="name">7am</div>
                  </div>
                  <div class="hour">
                    <div class="name">8am</div>
                  </div>
                  <div class="hour">
                    <div class="name">9am</div>
                  </div>
                  <div class="hour">
                    <div class="name">10am</div>
                  </div>
                  <div class="hour">
                    <div class="name">11am</div>
                  </div>
                  <div class="hour">
                    <div class="name">12pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">1pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">2pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">3pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">4pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">5pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">6pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">7pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">8pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">9pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">10pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">11pm</div>
                  </div>
                </div>
              </div>
              <div class="canvas"></div>
            </div>
            {/* time line hour end  */}
            {/* add offline botton start  */}
            <div class="add-offline-bottom">
              <span>
                <i class="fa fa-plus"></i>&nbsp;
                <Link to="" class="weaklink">
                  Add offline time
                </Link>
                &nbsp;&nbsp;&nbsp;&nbsp;
              </span>
              <span>
                <i class="fa fa-history"></i>
              <Link to=""  class="weaklink" href="/editlog/68627">
                  {" "}
                  History of changes
                </Link>
              </span>
            </div>
            {/* add ofline bottom end  */}
          </div>
          {/* body end  */}
        </div>
      </div>
    </div>
  );
}
