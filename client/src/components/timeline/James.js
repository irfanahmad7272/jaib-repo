import React, { useEffect, usestate } from "react";
import { Link } from "react-router-dom";
import "./BrainMay.css";
import Calendar from "react-calendar";
export default function James() {
  // const [state, setState] = usestate({
  //     date:new Date(),
  // })
  //    const onChangeDate = date =>{
  // setState((state)=>({
  //     ...state , date
  // }))
  //    }
  return (
    <div id="content_wrapper">
      <div id="content_wrapper_inner">
        <div id="react-view" >
          {/* //header  */}
          <div>
            <div class="timeline-header" id="page_title">
              <span class="__timezone-switch">
                All times are UTC-7
                <Link
                  to=""
                  class="fa fa-cog"
                  title=""
                  data-original-title="Change"
                >
                  <i></i>
                </Link>
              </span>
              <h1 id="handelTitle">
                <span class="empl-status offline"></span>&nbsp;James Hetfield &nbsp;
                <Link to="">
                  <i class="fa fa-cog" title="" data-original-title="Settings">
                    <i></i>
                  </i>
                </Link>
                <span id="never_work">never worked</span>
              </h1>
            </div>
          </div>
          {/* header end  */}

          {/* body start  */}
          <div id="content_wide" class="_timeline" style={{marginLeft:'3%'}}>
            {/* calender start  */}
            <div class="_timeline_calendar">
              {/* <Calendar
          onChange={(e)=>onChangeDate(e)}
          value={state.date}
        /> */}
            </div>
            {/* calender end  */}

            {/* calender time show start  */}
            <div class="_timeline_overview">
              <div class="info-outer">
                <table class="info">
                  <tbody>
                    <tr>
                      <td class="big" rowspan="1">
                        <div class="dur">4h 30m</div>
                      </td>
                      <td class="big2">
                        <div class="day">
                          Sunday, May 31
                          <span
                            title=""
                            data-original-title="Average Activity Level: NaN%"
                          >
                            <i></i>
                          </span>
                        </div>
                        <div class="bot">
                          <span>
                            Week &nbsp;
                            <Link
                              to=""
                              title=""
                              data-original-title="View report"
                            >
                              <i></i>0h 00m
                            </Link>
                          </span>
                          <b>●</b>
                          <span>
                            Month &nbsp;
                            <Link
                              to=""
                              title=""
                              data-original-title="View report"
                            >
                              <i></i>0h 00m
                            </Link>
                          </span>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div style={{marginLeft:'3%'}}>
            {/* calender time show end  */}
            {/* time line hour start  */}
            <div class="_timeline_hours">
              <div class="hours">
                <div class="hours-row">
                  <div class="hour">
                    <div class="name">12am</div>
                  </div>
                  <div class="hour">
                    <div class="name">1am</div>
                  </div>
                  <div class="hour">
                    <div class="name">2am</div>
                  </div>
                  <div class="hour">
                    <div class="name">3am</div>
                  </div>
                  <div class="hour">
                    <div class="name">4am</div>
                  </div>
                  <div class="hour">
                    <div class="name">5am</div>
                  </div>
                  <div class="hour">
                    <div class="name">6am</div>
                  </div>
                  <div class="hour">
                    <div class="name">7am</div>
                  </div>
                  <div class="hour">
                    <div class="name">8am</div>
                  </div>
                  <div class="hour">
                    <div class="name">9am</div>
                  </div>
                  <div class="hour">
                    <div class="name">10am</div>
                  </div>
                  <div class="hour">
                    <div class="name">11am</div>
                  </div>
                  <div class="hour">
                    <div class="name">12pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">1pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">2pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">3pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">4pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">5pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">6pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">7pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">8pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">9pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">10pm</div>
                  </div>
                  <div class="hour">
                    <div class="name">11pm</div>
                  </div>
                </div>
              </div>
              <div class="canvas"></div>
            </div>
            {/* time line hour end  */}

            {/* skype discussion start  */}
            <div class="_timeline_screens">
              <div class="activity" data-task-slug="administration-meeting">
                <Link class="head" to="">
                  <span class="time">6:20am ‐ 8:10am</span>
                  <span
                    class="activityLevel"
                    title="Average Activity Level: 29%"
                  >
                    <svg class="pie-progress" width="18" height="18">
                      <circle
                        cx="9"
                        cy="9"
                        r="7"
                        class="pie-progress__background"
                        stroke="#e27000"
                        fill="transparent"
                        stroke-width="2"
                      ></circle>
                      <path
                        d="M9 9 L9 2 A7 7 -90 0 1 15.780303875897523 10.739965330254899 z"
                        fill="#e27000"
                      ></path>
                    </svg>
                  </span>
                  <span class="project-label">Administration ● Amazon</span>
                  <span class="note"> Meeting</span>
                  <i class="fa fa-pencil"></i>
                </Link>
                {/* start images   */}
                <div class="screens">
                  <div class="timeline-screen">
                    <span class="time">
                      6:28am<span>PhpStorm</span>
                    </span>
                    <div
                      class="equals"
                      title="screentime is similar to previous"
                    ></div>
                    <span
                      title="Activity Level: 26%"
                      class="activityLevelContainer"
                    >
                      <svg class="pie-progress" width="18" height="18">
                        <circle
                          cx="9"
                          cy="9"
                          r="7"
                          class="pie-progress__background"
                          stroke="#e27000"
                          fill="transparent"
                          stroke-width="2"
                        ></circle>
                        {/* <path d="M9 9 L9 2 A7 7 -90 0 1 15.986201095950229 9.439311104963007 z" fill="#e27000"></path> */}
                      </svg>
                    </span>
                    <Link
                      class="delete fa fa-trash"
                      to=""
                      title="Delete screentime"
                    ></Link>
                    <Link
                      class="blur fa fa-paint-brush"
                      to=""
                      title="Blur screentime"
                    ></Link>
                    <div>
                      <Link
                        class="image"
                        to=""
                      >
                        <img
                          src="https://s3.amazonaws.com/screenshotmonitor-demo/05t.jpg"
                          class="timeline-scr"
                        />
                      </Link>
                    </div>
                  </div>
                  <div class="timeline-screen">
                    <span class="time">
                      6:37am<span>screentimemonitor.com</span>
                    </span>
                    <div
                      class="equals"
                      title="screentime is similar to previous"
                    ></div>
                    <span
                      title="Activity Level: 12%"
                      class="activityLevelContainer"
                    >
                      <svg class="pie-progress" width="18" height="18">
                        <circle
                          cx="9"
                          cy="9"
                          r="7"
                          class="pie-progress__background"
                          stroke="#C4381F"
                          fill="transparent"
                          stroke-width="2"
                        ></circle>
                        {/* <path d="M9 9 L9 2 A7 7 -90 0 1 13.793942340604168 3.8992042939397544 z" fill="#C4381F"></path> */}
                      </svg>
                    </span>
                    <Link
                      class="delete fa fa-trash"
                      to=""
                      title="Delete screentime"
                    ></Link>
                    <Link
                      class="blur fa fa-paint-brush"
                      to=""
                      title="Blur screentime"
                    ></Link>
                    <div>
                      <Link
                        class="image"
                        to=""
                      >
                        <img
                          src="https://s3.amazonaws.com/screenshotmonitor-demo/03t.jpg"
                          class="timeline-scr"
                        />
                      </Link>
                    </div>
                  </div>
                  <div class="timeline-screen">
                    <span class="time">
                      6:47am<span>sqlbackupandftp.com</span>
                    </span>
                    <div
                      class="equals"
                      title="screentime is similar to previous"
                    ></div>
                    <span
                      title="Activity Level: 23%"
                      class="activityLevelContainer"
                    >
                      <svg class="pie-progress" width="18" height="18">
                        <circle
                          cx="9"
                          cy="9"
                          r="7"
                          class="pie-progress__background"
                          stroke="#C4381F"
                          fill="transparent"
                          stroke-width="2"
                        ></circle>
                        {/* <path d="M9 9 L9 2 A7 7 -90 0 1 15.944858786587481 8.123109793441236 z" fill="#C4381F"></path> */}
                      </svg>
                    </span>
                    <Link
                      class="delete fa fa-trash"
                      to=""
                      title="Delete screentime"
                    ></Link>
                    <Link
                      class="blur fa fa-paint-brush"
                      to=""
                      title="Blur screentime"
                    ></Link>
                    <div>
                      <Link
                        class="image"
                        to=""
                      >
                        <img
                          src="https://s3.amazonaws.com/screenshotmonitor-demo/17t.jpg"
                          class="timeline-scr"
                        />
                      </Link>
                    </div>
                  </div>
                  <div class="timeline-screen">
                    <span class="time">
                      6:57am<span>screentimemonitor.com</span>
                    </span>
                    <div
                      class="equals"
                      title="screentime is similar to previous"
                    ></div>
                    <span
                      title="Activity Level: 30%"
                      class="activityLevelContainer"
                    >
                      <svg class="pie-progress" width="18" height="18">
                        <circle
                          cx="9"
                          cy="9"
                          r="7"
                          class="pie-progress__background"
                          stroke="#e27000"
                          fill="transparent"
                          stroke-width="2"
                        ></circle>
                      </svg>
                    </span>
                    <Link
                      class="delete fa fa-trash"
                      to=""
                      title="Delete screentime"
                    ></Link>
                    <Link
                      class="blur fa fa-paint-brush"
                      to=""
                      title="Blur screentime"
                    ></Link>
                    <div>
                      <Link
                        class="image"
                        to=""
                      >
                        <img
                          src="https://s3.amazonaws.com/screenshotmonitor-demo/12t.jpg"
                          class="timeline-scr"
                        />
                      </Link>
                    </div>
                  </div>
                  <div class="timeline-screen">
                    <span class="time">
                      7:07am<span>google.com</span>
                    </span>
                    <div
                      class="equals"
                      title="screentime is similar to previous"
                    ></div>
                    <span
                      title="Activity Level: 53%"
                      class="activityLevelContainer"
                    >
                      <svg class="pie-progress" width="18" height="18">
                        <circle
                          cx="9"
                          cy="9"
                          r="7"
                          class="pie-progress__background"
                          stroke="#50aa00"
                          fill="transparent"
                          stroke-width="2"
                        ></circle>
                        {/* <path d="M9 9 L9 2 A7 7 -90 1 1 7.694463936576508 15.877177879559369 z" fill="#50aa00"></path> */}
                      </svg>
                    </span>
                    <Link
                      class="delete fa fa-trash"
                      to=""
                      title="Delete screentime"
                    ></Link>
                    <Link
                      class="blur fa fa-paint-brush"
                      to=""
                      title="Blur screentime"
                    ></Link>
                    <div>
                      <Link
                        class="image"
                        to=""
                      >
                        <img
                          src="https://s3.amazonaws.com/screenshotmonitor-demo/04t.jpg"
                          class="timeline-scr"
                        />
                      </Link>
                    </div>
                  </div>
                  <div class="timeline-screen">
                    <span class="time">
                      7:17am<span>trello.com</span>
                    </span>
                    <div
                      class="equals"
                      title="screentime is similar to previous"
                    ></div>
                    <span
                      title="Activity Level: 40%"
                      class="activityLevelContainer"
                    >
                      <svg class="pie-progress" width="18" height="18">
                        <circle
                          cx="9"
                          cy="9"
                          r="7"
                          class="pie-progress__background"
                          stroke="#e27000"
                          fill="transparent"
                          stroke-width="2"
                        ></circle>
                        {/* <path d="M9 9 L9 2 A7 7 -90 0 1 13.117202112319625 14.661152423871911 z" fill="#e27000"></path> */}
                      </svg>
                    </span>
                    <Link
                      class="delete fa fa-trash"
                      to=""
                      title="Delete screentime"
                    ></Link>
                    <Link
                      class="blur fa fa-paint-brush"
                      to=""
                      title="Blur screentime"
                    ></Link>
                    <div>
                      <Link
                        class="image"
                        to=""
                      >
                        <img
                          src="https://s3.amazonaws.com/screenshotmonitor-demo/14t.jpg"
                          class="timeline-scr"
                        />
                      </Link>
                    </div>
                  </div>
                  <div class="timeline-screen">
                    <span class="time">
                      7:26am<span>Remote Desktop Connection</span>
                    </span>
                    <div
                      class="equals"
                      title="screentime is similar to previous"
                    ></div>
                    <span
                      title="Activity Level: 36%"
                      class="activityLevelContainer"
                    >
                      <svg class="pie-progress" width="18" height="18">
                        <circle
                          cx="9"
                          cy="9"
                          r="7"
                          class="pie-progress__background"
                          stroke="#e27000"
                          fill="transparent"
                          stroke-width="2"
                        ></circle>
                        {/* <path d="M9 9 L9 2 A7 7 -90 0 1 14.395155769548547 13.460077826933857 z" fill="#e27000"></path> */}
                      </svg>
                    </span>
                    <Link
                      class="delete fa fa-trash"
                      to=""
                      title="Delete screentime"
                    ></Link>
                    <Link
                      class="blur fa fa-paint-brush"
                      to=""
                      title="Blur screentime"
                    ></Link>
                    <div>
                      <Link
                        class="image"
                        to=""
                      >
                        <img
                          src="https://s3.amazonaws.com/screenshotmonitor-demo/03t.jpg"
                          class="timeline-scr"
                        />
                      </Link>
                    </div>
                  </div>
                  <div class="timeline-screen">
                    <span class="time">
                      7:36am<span>localhost</span>
                    </span>
                    <div
                      class="equals"
                      title="screentime is similar to previous"
                    ></div>
                    <span
                      title="Activity Level: 8%"
                      class="activityLevelContainer"
                    >
                      <svg class="pie-progress" width="18" height="18">
                        <circle
                          cx="9"
                          cy="9"
                          r="7"
                          class="pie-progress__background"
                          stroke="#C4381F"
                          fill="transparent"
                          stroke-width="2"
                        ></circle>
                        {/* <path d="M9 9 L9 2 A7 7 -90 0 1 12.375596878224256 2.867680233735144 z" fill="#C4381F"></path> */}
                      </svg>
                    </span>
                    <Link
                      class="delete fa fa-trash"
                      to=""
                      title="Delete screentime"
                    ></Link>
                    <Link
                      class="blur fa fa-paint-brush"
                      to=""
                      title="Blur screentime"
                    ></Link>
                    <div>
                      <Link
                        class="image"
                        to=""
                      >
                        <img
                          src="https://s3.amazonaws.com/screenshotmonitor-demo/04t.jpg"
                          class="timeline-scr"
                        />
                      </Link>
                    </div>
                  </div>
                  <div class="timeline-screen">
                    <span class="time">
                      7:46am<span>Xcode</span>
                    </span>
                    <div
                      class="equals"
                      title="screentime is similar to previous"
                    ></div>
                    <span
                      title="Activity Level: 37%"
                      class="activityLevelContainer"
                    >
                      <svg class="pie-progress" width="18" height="18">
                        <circle
                          cx="9"
                          cy="9"
                          r="7"
                          class="pie-progress__background"
                          stroke="#e27000"
                          fill="transparent"
                          stroke-width="2"
                        ></circle>
                      </svg>
                    </span>
                    <Link
                      class="delete fa fa-trash"
                      to=""
                      title="Delete screentime"
                    ></Link>
                    <Link
                      class="blur fa fa-paint-brush"
                      to=""
                      title="Blur screentime"
                    ></Link>
                    <div>
                      <Link
                        class="image"
                        to=""
                      >
                        <img
                          src="https://s3.amazonaws.com/screenshotmonitor-demo/11t.jpg"
                          class="timeline-scr"
                        />
                      </Link>
                    </div>
                  </div>
                </div>
                {/* ens images  */}
              </div>
            </div>
            </div>
            {/* skype discussion end  */}
            {/* add offline botton start  */}
            <div class="add-offline-bottom">
              <span>
                <i class="fa fa-plus"></i>&nbsp;
                <Link to="" class="weaklink">
                  Add offline time
                </Link>
                &nbsp;&nbsp;&nbsp;&nbsp;
              </span>
              <span>
                <i class="fa fa-history"></i>
                <Link to="" class="weaklink" to="/editlog/68627">
                  {" "}
                  History of changes
                </Link>
              </span>
            </div>
            {/* add ofline bottom end  */}
          </div>
          {/* body end  */}
        </div>
      </div>
    </div>
  );
}
