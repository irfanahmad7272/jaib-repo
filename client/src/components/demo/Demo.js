import React from "react";
import {Link} from 'react-router-dom'
import "./demo.css";

export default function Demo() {
  return (
    <div id="content_wrapper">
      <div id="content_wrapper_inner">
        <div id="react-view">
          <div>
            <div id="page_title">
              <span class="__timezone-switch">
                All times are UTC-7
                <Link
                  class="fa fa-cog"
                  title=""
                  data-original-title="Change"
                >
                 
                </Link>
              </span>
         
              <h1 >Manager Dashboard</h1>
            </div>
            <div id="content_wide" class="_index">
              <table class="empls">
                <tbody>
                  <tr>
                    <th>
                      <Link  class="order-link">
                        <u>Employee</u>
                      </Link>
                    </th>
                    <th class="lastworked">
                      <Link  class="order-link">
                        <u>Last active</u>
                        <i class="fa fa-caret-up"></i>
                      </Link>
                    </th>
                    <th class="daywork">Today</th>
                    <th class="daywork">Yesterday</th>
                    <th class="daywork">This week</th>
                    <th class="daywork">This month</th>
                  </tr>
                  <tr id="handelSecondTr">
                    <td>2 online, 4 worked today</td>
                    <td></td>
                    <td style={{textAlign:'center'}}>
                      <Link>16h 39m</Link>
                      <br />
                      <span id="handelSecondTr1">$144</span>
                    </td>
                    <td style={{textAlign:'center'}}>
                      <Link >27h 36m</Link>
                      <br />
                      <span id="handelSecondTr1">$235</span>
                    </td>
                    <td style={{textAlign:'center'}}>
                      <Link >108h 09m</Link>
                      <br />
                      <span id="handelSecondTr1">$956</span>
                    </td>
                    <td style={{textAlign:'center'}}>
                      <Link >169h 11m</Link>
                      <br />
                      <span id="handelSecondTr1">$1,491</span>
                    </td>
                  </tr>
                <tr class="empl">
                    <td>
                      <div class="name">
                        <span class="empl-status online"></span>&nbsp;
                        <Link to="/james">James Hetfield</Link>
                        <div id="idSpan">
                          <span class="project-label">
                            Administration ● Amazon
                          </span>
                          <span class="noteLabel">Support</span>
                        </div>
                      </div>
                    </td>
                    <td class="lastworked">
                      <Link to="/james" class="_myhome-screen" id="68628" >
                        <span>a minute ago</span>
                        <img width= "180px" height="101px" src="https://s3.amazonaws.com/screenshotmonitor-demo/19t.jpg" />
                      </Link>
                    </td>
                    <td class="daywork">
                      <Link >4h 07m</Link>
                      <br />
                      <div title="" class="range-money">
                        $41
                      </div>
                    </td>
                    <td class="daywork">
                      <Link >6h 44m</Link>
                      <br />
                      <div title="" class="range-money">
                        $67
                      </div>
                    </td>
                    <td class="daywork">
                      <Link >25h 28m</Link>
                      <br />
                      <div title="" class="range-money">
                        $255
                      </div>
                    </td>
                    <td class="daywork">
                      <Link >39h 33m</Link>
                      <br />
                      <div title="" class="range-money">
                        $396
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
