import React , {useEffect} from "react";
// import logo from './logo.svg';
import "./App.css";
import Header from "./components/header/header";
import { Route, BrowserRouter, Redirect } from "react-router-dom";

import Home from "./components/myhome/Home";
 import Demo from "./components/demo/Demo";
import Footer from "./components/footer/Footer";
import HowItsWork from "./components/HowItWork/howItsWork";
import Pricing from "./components/pricing/Pricing";
import Download from "./components/download/Download";
import Support from "./components/support/Support";
import Blog from "./components/blog/blog";
import SaveReports from "./components/reports/saveReports";
import Summary from "./components/reports/summary";
import Team from "./components/team/Team";
import Client from "./components/client/client";
import ReportSetting from "./components/setting/setting";
import BrainMay from "./components/timeline/BrainMay";
import DemoManager from "./components/timeline/demoManager";
import James from "./components/timeline/James";
import Till from "./components/timeline/Till";
import Victor from "./components/timeline/Victor";
import "react-calendar/dist/Calendar.css";
import Signup from "./components/signup/signup";
import Login from "./components/login/login";
import { Provider } from "react-redux";
import store from "./redux/store";
import Profile from "./components/account/profile";
// import Demo from "./components/Demo";
import ActivityLevelTracking from "./components/setting/ActivityLevelTracking";
import Screenshorts from "./components/setting/screenshorts";
import setBasePath from './utils/setbasePath';
import setAuthToken from './utils/setAuthToken'
import {loadUser} from './redux/actions/authAction';
import {connect} from 'react-redux'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ShowProfile from "./components/account/ShowProfile";

function App() {
  // useEffect(() => {
  //     setBasePath()
  //   let loaduser = localStorage.getItem('token')
  //    loadUser(loaduser)
  //    setAuthToken(loaduser)
  //   // console.log('loaduser',loaduser)
  // });
  return (
    // <div className="App">
    <div>
      <Provider store={store}>
        <BrowserRouter>
        <ToastContainer />
          <Header />
          <Route exact path="/" component={Home} />
          <Route exact path="/profile" component={Profile} />

          <Route exact path="/signup" component={Signup} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/myhome" component={Demo} />
          <Route exact path="/tour" component={HowItsWork} />
          <Route exact path="/pricing" component={Pricing} />
          <Route exact path="/download_win" component={Download} />
          <Route exact path="/support" component={Support} />
          <Route exact path="/blog" component={Blog} />
          <Route exact path="/savereports" component={SaveReports} />
          <Route exact path="/summary" component={Summary} />
          <Route exact path="/report_details" component={Summary} />
          <Route exact path="/team" component={Team} />
          <Route exact path="/client" component={Client} />
          <Route exact path="/setting" component={ReportSetting} />
          <Route exact path="/setting/screenshorts" component={Screenshorts} />
          <Route exact path="/setting/ActivityLevelTracking" component={ActivityLevelTracking} />
          <Route exact path="/account" component={ShowProfile} />


          <Route exact path="/BrainMay" component={BrainMay} />
          <Route exact path="/DemoManager" component={DemoManager} />
          <Route exact path="/james" component={James} />
          <Route exact path="/till" component={Till} />
          <Route exact path="/victor" component={Victor} />
          <Footer />
        </BrowserRouter>
      </Provider>
    </div>
  );
}
const mapStateToProps = state => ({
  auth: state.auth
});
const mapDispatchToProps = dispatch =>{
  return {
    loadUser : data => dispatch(loadUser(data))
  }
}
export default App;
