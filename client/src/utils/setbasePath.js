import axios from "axios";

const setBasePath = () => { 
  
  //live link
      axios.defaults.baseURL = "http://deskplay.net/screen_laravel/public/api";
 
  //test Link
  //  axios.defaults.baseURL = "http://order.rafhanmaize.com/dev/public/api";
};
export default setBasePath; 