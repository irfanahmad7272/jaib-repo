import { combineReducers } from 'redux';


import profile from './ProfileReducer';
import auth from './authReducer';
export default combineReducers({
    auth,
    profile
})