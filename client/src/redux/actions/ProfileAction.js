import axios from "axios";
import { toast } from "react-toastify";
// Register User
export const RegisterProfile = (data) => (dispatch) => {
  console.log("register", data);
  // Headers
  let getToken = localStorage.getItem("token");
  //  console.log('getToken',getToken)
  const token = {
    headers: {
      Authorization: "Bearer " + getToken,
      Accept: "application/json",
    },
  };
  axios
    .post("/profile", data, token)
    .then((res) => {
      //    console.log('register profile', res.data)
      toast.success("Register Profile Successfully");
      dispatch({
        type: "REGISTER_SUCCESS_PROFILE",
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("register err", err);
      // dispatch(
      //   returnErrors(err.response.data, err.response.status, 'REGISTER_FAIL')
      // );
      toast.success("Unauthorized User, Login First");
      dispatch({
        type: "REGISTER_FAIL",
      });
    });
};


// Check token & load user
export const loadUserProfile = (data)  => dispatch => {
    console.log('data', data)
    // alert('loadUserProfile')
    //  console.log('data bearer', data)
      // User loading
      // dispatch({ type:"USER_LOADING" });
      const token = {
        headers:{
        'Authorization': 'Bearer ' +data,
       'Accept': 'application/json'
        }
      }
      axios
        .get('/profile',token)
        .then(res =>  { 
          //  alert('load user')
          // console.log("response.headers",res.headers);
     console.log('responce load user', res.data)
          dispatch({
            type: "USER_LOADED_PROFILE",
            payload: res.data
          })
        }
        )
        .catch(err => {
            console.log('err auth', err)
          // dispatch(returnErrors(err.response.data, err.response.status));
          dispatch({
            type: "AUTH_ERROR"
          });
        });
    };